import React, { Component } from "react";
import { BrowserRouter as Router } from 'react-router-dom';
import {LayoutRoutes} from "./Routes";

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

document.body.style = 'background: #f7f8fc;';

class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <React.Suspense fallback={loading()}>
            <LayoutRoutes />
          </React.Suspense>
        </Router>
      </div>
    );
  }
}

export default App;
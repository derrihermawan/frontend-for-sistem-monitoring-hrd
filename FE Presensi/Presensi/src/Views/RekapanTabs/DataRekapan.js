import React, { useState } from "react";

import { Container, Card, CardContent, Link } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

import {DataRekapanPresensi} from "./DataRekapanContent";


//var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

const useStyles = makeStyles(theme => ({
    linkNavigationContainer: {
        padding: 0,
        width: "100%",
        display: "flex",
        alignItems: "left",
        justifyItems: "flex-start",
    },
    linkNavigation: {
        margin: 10 + "pt",
        fontSize: 12 + "pt",
    },
    linkNavigationActive:{
        borderBottom: "2pt solid #0182C6"
    },
    contentCard: {

    }
}));

function DataRekapan(){
    const classes = useStyles();
    const [activeContent, setActiveContent]=useState("DataRekapanPresensi");

    function handleClick(linkNav){
        setActiveContent(linkNav);
    }

    return(
        <div>
            <Container maxWidth={false} className={classes.linkNavigationContainer}>
                <Link className={classes.linkNavigation + ((activeContent === "DataRekapanPresensi") ? " " + classes.linkNavigationActive + " mb-2" : "")} onClick={() => (handleClick("DataRekapanPresensi"))}>Data Rekapan Presensi</Link>
                
            </Container>
            <Card raised={false} className={classes.contentCard}>
                <CardContent>
                    {
                        (activeContent === "DataRekapanPresensi") ? <DataRekapanPresensi /> :
                                null
                    }
                </CardContent>
            </Card>
        </div>
    );
}

export default DataRekapan;
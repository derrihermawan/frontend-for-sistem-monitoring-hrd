import React, {useState, useEffect} from "react";

import Card from '@material-ui/core/Card';
import { Container, Typography, Grid } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import DataAttendance from "./PresensiTabs/DataAttendance";

var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

const activeCardNavStyle = { /*borderBottom: "solid 0px #0182C6",*/ background: "#fdfeff", color: "#0f0f0f" };
const useStyles = makeStyles(theme => ({
    mainContainer:{
        padding: theme.spacing(1, 0),
    },
    navContainer: {
        padding: theme.spacing(2,0),
    },
    cardContainer: {
        padding: theme.spacing(0,2),
        marginBottom: 10+"pt",
    },
    navigationCard: {
        //height: (h*8.5/100)+"pt",
        maxHeight: 375+"px",
        padding: 0,
    },
    navTitle: {
        textAlign: "center",
        //fontSize: (h*w * 0.0012 / 100)+"px",
        fontSize: 12+"pt",
        color: "#656565",
    },
    linkNavigationContainer: {
        padding: 0,
        width: "100%",
        display: "flex",
        alignItems: "left",
        justifyItems: "flex-start",
    },
    linkNavigation: {
        margin: 10+"pt",
        fontSize: (h * 1.35 / 100)+"pt",
    },
    cursorPointer: {
        cursor: "pointer",
    },
    contentCard: {
        background: `url("${process.env.PUBLIC_URL}/bg_eggshell.png")`,
    }
}));



function Presensi(){
    const classes = useStyles();
    return(
        <Grid container direction="row" alignItems="center" justify="center" className={classes.mainContainer}>
            <Grid item container direction="row" alignItems="center" justify="center" className={classes.navContainer} style={{ width: 100 + "%" }}>
                <Grid item className={classes.cardContainer} style={{ width: 100 + "%" }}>
                    <Card raised={false} className={classes.contentCard}>
                        <DataAttendance />
                    </Card>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Presensi;

import 'date-fns';
import React, { useEffect } from "react";
//import MaskedInput from 'react-text-mask'
import PropTypes from 'prop-types';
import clsx from 'clsx';
import DetailKaryawan from "../../KepegawaianTabs/KaryawanContent/DetailKaryawan"
import * as apiDataNotPresent from "../../../Services/apiDataNotPresent";
import * as apiDataKaryawan from "../../../Services/apiDataKaryawan";
import * as optionLists from "../../../Services/optionLists";
import { useTableData } from "../../../Hooks";
import Moment from 'react-moment';
import moment from 'moment';
import { MenuItem, Select, FormControl, makeStyles, Button ,LinearProgress, CircularProgress, withStyles, IconButton, InputAdornment, Input, Box, Tooltip, TablePagination, Paper, Table, TableHead, TableRow, TableBody, TableCell, Drawer, Link, Container, Typography,  SnackbarContent } from "@material-ui/core";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    DatePicker
} from '@material-ui/pickers';
import SaveIcon from '@material-ui/icons/Save';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import SearchIcon from "@material-ui/icons/Search";
import SortIcon from '@material-ui/icons/Sort';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import { amber, green } from '@material-ui/core/colors';
import WarningIcon from '@material-ui/icons/Warning';
import Swal from 'sweetalert2';

var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
let timeoutPromise;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(0.5),
    },
    filterSelect: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    filterInput: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    toolbar: {
        borderBottom: "1px solid #e0e0e0",
        height: (h * 5 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(4, 0),
    },
    toolbarFoot: {
        borderTop: "1px solid #e0e0e0",
        height: (h * 2 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(3, 0),
        marginTop: 5 + "pt",
    },
    searchButton: {
        height: (h * 2.5 / 100) + "px",
        width: (h * 2.5 / 100) + "px",
    },
    rotateHalf: {
        transform: "rotateX(180deg)",
    },
    tableRoot: {
        margin: theme.spacing(2, 0),
    },
    table: {
        minWidth: 650,
    },
    drawerModal: {
        padding: theme.spacing(0),
        overflowX: "hidden",
        minWidth: 500 + "px",
    },
    pageTitle: {
        marginLeft: theme.spacing(5),
        fontSize: 18 + "pt",
    },
    modalContent: {
        padding: theme.spacing(0, 5),
        paddingBottom: theme.spacing(10),
        marginTop: 10 + "pt",
    },
    buttonEdit: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#ffffff",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonDelete: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#FFFFFF",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonContainer: {
        width: 115 + "pt",
        marginLeft: 50 + "%",
        transform: "translate(-50%, 0)",
    },
    buttonControlContainer: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(5),
        width: "auto"
    },
    tableHeader: {
        borderBottom: "2px solid #e0e0e0",
        "& th": {
            fontSize: 11 + "pt",
            fontWeight: "bold",
        },
    },
    buttonControl: {
        fontSize: 8.75 + "pt",
        width: theme.spacing(13),
        padding: theme.spacing(1, 3),
    },
    chipAvatar: {
        height: 42.5 + "px", width: 42.5 + "px",
    },
    textField: {
        width: 100 + "%",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        margin: theme.spacing(0),
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
    },
}));

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 5000
});

// Components
function DetailData(props) {
    const classes = useStyles();

    const [details, setDetails] = React.useState({});
    const [childDatas, setChildDatas] = React.useState({ "items": [] });

    useEffect(() => {
        let tempData = {};
        var errorStatus = false;

        apiDataKaryawan.getData("nip=" + (props.id), "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setDetails((details) =>
                    ({
                        ...details,
                        
                        nip: tempData.data.items[0].nip,
                        name: tempData.data.items[0].name,
                        status: tempData.data.items[0].status,
                        religion: tempData.data.items[0].religion,
                        gender: tempData.data.items[0].gender,
                        placeOfBirth: tempData.data.items[0].placeOfBirth,
                        dateOfBirth: tempData.data.items[0].dateOfBirth,
                        address: tempData.data.items[0].address,
                        phoneNumber: tempData.data.items[0].phoneNumber,
                        postalCode: tempData.data.items[0].postalCode,
                        
                    }));

            }
        });

        
    }, [props.id]);

    return (
        <div className={classes.modalContent}>
            <div className="row">
                <div className="col-auto mt-2">
                <span className="font-weight-bold">NIP</span>
                </div>
                <div className="col">
                    <div className="col-12 mt-2">
                        <span className="font-weight-bold">{details.nip}</span>
                    </div>
                </div>
            </div>
            
            
            <div className="row mt-3">
                <div className="col-5">
                    <span className="text-secondary-custom">Name</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.name}</span>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-5">
                    <span className="text-secondary-custom">Status</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.status}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Tempat Tanggal Lahir</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.placeOfBirth}, <Moment>{details.dateOfBirth}</Moment> </span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Jenis Kelamin</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.gender}</span>
                </div>
            </div>
            
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Agama</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.religion}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Alamat</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.address} {details.postalCode}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">No Telepon</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.phoneNumber}</span>
                </div>
            </div>
        </div>
    );
}

const CustomCircularProgress = withStyles({
    root: {
        color: '#517296',
    },
})(CircularProgress);
const CustomLinearProgress = withStyles({
    colorPrimary: {
        height: 4 + "px",
        borderRadius: "0px 0px 4px 4px",
        backgroundColor: '#d3d3d3',
    },
    barColorPrimary: {
        borderRadius: 25 + "px",
        backgroundColor: '#517296',
    },
})(LinearProgress);

// SnackBar
const LoadingIcon = withStyles(theme => ({
    root: {
        color: amber[100],
    },
}))(CircularProgress);
/*
LoadingIcon.defaultProps={
    size: 25,
    thickness: 4,
}
//*/
const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
    "info-process": WarningIcon,
};
const useSnackBarStyles = makeStyles(theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    "info-process": {
        backgroundColor: amber[800],
        color: theme.palette.common.white,
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));
function CustomSnackbarContent(props) {
    const classes = useSnackBarStyles();
    const { className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={clsx(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.iconVariant)} />
                    {message}
                    {(variant === "info-process") ? <LoadingIcon style={{ position: "absolute", right: 20 + "px" }} size={25} thickness={4} /> : null}
                </span>
            }
            {...other}
        />
    );
}
CustomSnackbarContent.propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['error', 'info', 'success', 'warning', 'info-process']).isRequired,
}
// SnackBar

// Components

function DataNotPresent() {
    const classes = useStyles();
    
    const [filterBy, setFilterBy] = React.useState("nip");
    function handleChangeFilter(event) {
        setFilterBy(event.target.value);
    }

    const [filterValue, setFilterValue] = React.useState("");
    function handleChangeFilterValue(event) {
        setFilterValue(event.target.value);
    }


    const [sortBy, setSortBy] = React.useState("date");
    function handleChangeSortBy(event) {
        setSortBy(event.target.value);
    }

    const [isSortAscending, setSortAscending] = React.useState(false);
    function handleChangeSortAscending() {
        setSortAscending(isSortAscending);
        
    }
    const [isSortDescending, setSortDescending] = React.useState(true);
    function handleChangeSortDescending() {
        setSortDescending(!isSortDescending);       
    }


    const [page, setPage] = React.useState(0);
    function handleChangePage(event, newPage) {
        setPage(newPage);
    }

    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    function handleChangeRowsPerPage(event) {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
        delete: false,
    });
    //export pdf
    function exportPdf(event) {
        const unit = "pt";
        const size = "A4"; // Use A1, A2, A3 or A4
        const orientation = "portrait"; // portrait or landscape
    
        const marginLeft = 40;
        const doc = new jsPDF(orientation, unit, size);
    
        doc.setFontSize(15);
    
        const title = "NOT PRESENCE EMPLOYEE";
        const headers = [["NIP", "NAME", "STATUS", "DATE"]];
    
        const data = tableDatas.items.map(elt=> [elt.nip, elt.name, elt.status, moment(elt.date).format("DD MMM YYYY")]);
    
        let content = {
          startY: 50,
          head: headers,
          body: data
        };
    
        doc.text(title, marginLeft, 40);
        doc.autoTable(content);
        doc.save("Not Presence Report.pdf")
      }

    const toggleDrawer = (side, open) => event => {
        setState({ ...state, [side]: open });
        setSelectedNipEmployee(selectedNipEmployee || "");
    };
    const toggleDrawerModalContent = (side, open, modalContent, selectedNipEmployee) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setState({ ...state, [side]: open });
        setModalContent(modalContent);
        setSelectedNipEmployee(selectedNipEmployee || "");
    };
    const [startDate, handleStartDateChange] = React.useState(new Date());
    const [endDate, handleEndDateChange] = React.useState(new Date());
    const [modalContent, setModalContent] = React.useState("");
    const [selectedNipEmployee, setSelectedNipEmployee] = React.useState("");
    const [filterOl, setFilterOl] = React.useState({
        "data": [
            { "key": "1", "values": { "key": "NIP", "value": "nip" } },
            { "key": "2", "values": { "key": "Nama Karyawan", "value": "name" } },
            { "key": "3", "values": { "key": "Status", "value": "status" } },
            { "key": "4", "values": { "key": "Tanggal", "value": "date" } },
        ]
    });
    const { tableDatas, setTableDatas } = useTableData({ "totalItems": 0, "items": [] });
    const [isFetching, setFetching] = React.useState(true);

    useEffect(() => {
        if (!state.top && !state.left && !state.bottom && !state.right && !state.delete) {
            setFetching(true);
            let dateStart = startDate.getFullYear() + "-" + (startDate.getMonth()+1) + "-" + startDate.getDate();
            let dateEnd = endDate.getFullYear() + "-" + (endDate.getMonth()+1) + "-" + endDate.getDate(); 
            console.log("daa "+dateStart);
            let tempData = {};
            let errorObj = {};
            var errorStatus = false;
            apiDataNotPresent.cancel && apiDataNotPresent.cancel();
            var requestPromise = apiDataNotPresent.getData(`date >= '${dateStart}' and date <= '${dateEnd}'`,((filterValue !== "") ? `and lower(cast(${filterBy} as string)) LIKE lower('%25${filterValue}%25')` : ""),`${sortBy} ${(isSortDescending) ? "DESC" : "ASC"}`, page, rowsPerPage);
            requestPromise.then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
                errorObj = error;
            }).finally(() => {
                if (!errorStatus) { 
                    setTableDatas((data) => ({ ...data, "totalItems": tempData.data.totalItems, "items": tempData.data.items }));
                }
                if (!errorObj.__CANCEL__) {
                    setFetching(false);
                }
            });
        }
    }, [filterValue, filterBy, isSortAscending, isSortDescending, sortBy, page, rowsPerPage, setTableDatas, state, setFetching, startDate, endDate]);

    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        optionLists.getOlDataOrtu("", "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setFilterOl((data) => ({ ...data, "data": tempData.data }));
            }
        });
    }, [setFilterOl]);


    //Cleanup function
    useEffect(() => {
        return function cleanUp() {
            if (apiDataNotPresent.cancel && apiDataNotPresent.cancel()) {
                apiDataNotPresent.cancel = null;
            }

            if (optionLists.cancel && optionLists.cancel()) {
                console.log("AAA");
                optionLists.cancel = null;
            }
        }
    }, []);
    Moment.globalFormat = 'YYYY MMM DD'
    return (
        <div>
            <Box className={classes.toolbar}>

                <Box flexGrow={1}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <Select
                            value={filterBy}
                            onChange={handleChangeFilter}
                            name="filterBy"
                            displayEmpty
                            className={classes.filterSelect}>
                            {(filterOl.data === null || filterOl.data.length === 0) ?
                                <MenuItem>filter</MenuItem> :
                                filterOl.data.map(row => (
                                    <MenuItem key={row.key} value={row.values.value}>{row.values.key}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <FormControl className={clsx(classes.formControl)}>
                        <Input
                            value={filterValue}
                            onChange={handleChangeFilterValue}
                            className={classes.filterInput}
                            placeholder="Filter"
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton>
                                        <SearchIcon className={classes.searchButton} />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Box>
                <FormControl variant="outlined" className={classes.formControl}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DatePicker
                            disableFuture
                            openTo="year"
                            format="yyyy-MM-dd"
                            label="Start Date"
                            views={["year", "month", "date"]}
                            value={startDate}
                            onChange={handleStartDateChange}
                        />
                    </MuiPickersUtilsProvider>
                </FormControl>
                <FormControl variant="outlined" className={classes.formControl}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DatePicker
                            disableFuture
                            openTo="year"
                            format="yyyy-MM-dd"
                            label="End Date"
                            views={["year", "month", "date"]}
                            value={endDate}
                            onChange={handleEndDateChange}
                        />
                    </MuiPickersUtilsProvider>
                </FormControl>

                <FormControl variant="outlined" className={classes.formControl}>
                    <Tooltip title={isSortDescending ? "Descending" : "Ascending"}>
                        <IconButton onClick={handleChangeSortDescending}>
                            <SortIcon className={isSortDescending ? "" : classes.rotateHalf} />
                        </IconButton>
                    </Tooltip>
                </FormControl>
               
                <FormControl variant="outlined" className={classes.formControl}>
                    <Select
                        value={sortBy}
                        onChange={handleChangeSortBy}
                        name="sortBy"
                        displayEmpty
                        className={classes.filterSelect}>

                        {(filterOl.data === null || filterOl.data.length === 0) ? <MenuItem>filter</MenuItem> :
                            filterOl.data.map(row => (
                                <MenuItem key={row.key} value={row.values.value}>{row.values.key}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>

            </Box>

            <Paper className={classes.tableRoot}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow className={classes.tableHeader}>

                            <TableCell>NIP</TableCell>
                            <TableCell align="left">Nama Karyawan</TableCell>
                            <TableCell align="center">Status</TableCell>
                            <TableCell align="center">Tanggal</TableCell>
 

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(tableDatas.items === null || tableDatas.items.length === 0) ? <TableRow><TableCell align="center" colSpan={6}>{(isFetching) ? "Sedang Memuat ..." : "Tidak Ada Data"}</TableCell></TableRow> :
                            tableDatas.items.map(row => (
                                
                            <TableRow key={row.rowNumber}>
                            <TableCell component="th">
                              <Link onClick={toggleDrawerModalContent("right", true, "detail", row)} style={{ color: "#48d" }}>{row.nip}</Link>
                            </TableCell>
                            <TableCell align="left">{row.name}</TableCell>
                            <TableCell align="center" >{row.status}</TableCell>
                            <TableCell align="center" ><Moment>{row.date}</Moment></TableCell>
                                    
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
                {(isFetching) ? <CustomLinearProgress /> : null}
            </Paper>

            <div>
                <Button
                        onClick = {exportPdf}
                        variant="contained"
                        color="primary"
                        size="medium"
                        className={classes.button}
                        startIcon={<SaveIcon />}
                        >
                        Save
                </Button>
                <Box className={classes.toolbarFoot}>
                    
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25, 50, 100]}
                        component="div"
                        count={tableDatas.totalItems}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                            'aria-label': 'previous page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'next page',
                        }}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Box>
                <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false)}>
                    <Container maxWidth={"lg"} className={classes.drawerModal}>
                        <Typography className={classes.pageTitle}>
                            {
                                (modalContent === "edit") ? "Ubah Data Guru" :
                                    (modalContent === "tambah") ? "Tambah Data Guru" :
                                         "Employee Detail" 
                                        
                            }
                        </Typography>
                        {
                                    <DetailKaryawan row={selectedNipEmployee} />
                        }
                    </Container>
                </Drawer>
            </div>
        </div>
    );
}

export default DataNotPresent;
import React, { useState } from "react";

import { Container, Card, CardContent, Link, Button } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import * as DataTerlambat from "./DataTerlambatContent/DataTerlambat";
import {DataTepatWaktu} from "./DataTepatWaktuContent";
import {DataTidakMasuk} from "./DataTidakMasukContent";
import jsPDF from 'jspdf';
import 'jspdf-autotable';
//var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);


const useStyles = makeStyles(theme => ({
    linkNavigationContainer: {
        padding: 0,
        width: "100%",
        display: "flex",
        alignItems: "right",
        justifyItems: "flex-start",
    },
    linkNavigation: {
        margin: 10 + "pt",
        fontSize: 12 + "pt",
    },
    linkNavigationActive:{
        borderBottom: "2pt solid #0182C6"
    },
    contentCard: {

    }
}));



function DataAttendance(){
    const classes = useStyles();
    const [activeContent, setActiveContent]=useState("DataTerlambat");
    const [isDownload, setIsDownload]=useState(false);
    function handleDownload(statusDownload){
        if(activeContent === "DataTepatWaktu"){

        }
        if(activeContent === "DataTerlambat"){
            DataTerlambat.DownloadReport()
        }
        if(activeContent === "DataTidakMasuk"){

        }

    }
    function handleClick(linkNav){
        setActiveContent(linkNav);
    }

    return(
        <div>
            <Container maxWidth={false} className={classes.linkNavigationContainer}>
                <Link className={classes.linkNavigation + ((activeContent === "DataTerlambat") ? " " + classes.linkNavigationActive + " mb-2" : "")} onClick={() => (handleClick("DataTerlambat"))}>Data Terlambat</Link>
                <Link className={classes.linkNavigation + ((activeContent === "DataTepatWaktu") ? " " + classes.linkNavigationActive + " mb-2" : "")} onClick={() => (handleClick("DataTepatWaktu"))}>Data Tepat Waktu</Link>
                <Link className={classes.linkNavigation + ((activeContent === "DataTidakMasuk") ? " " + classes.linkNavigationActive + " mb-2" : "")} onClick={() => (handleClick("DataTidakMasuk"))}>Data Tidak Hadir</Link>
                <Button className={classes.linkNavigation + ((isDownload === "true") ? " " + classes.linkNavigationActive + " mb-2" : "" + " ml-auto")} onClick={() =>  (handleDownload(true))}
                ><GetAppIcon/></Button>
                
            </Container>
            <Card raised={false} className={classes.contentCard}>
                <CardContent>
                    {
                        (activeContent === "DataTepatWaktu") ? <DataTepatWaktu /> :
                                (activeContent === "DataTerlambat") ? <DataTerlambat.DataTerlambat/>  : 
                                        (activeContent === "DataTidakMasuk") ? <DataTidakMasuk /> : null
                    }
                </CardContent>
            </Card>
        </div>
    );
}

export default DataAttendance;
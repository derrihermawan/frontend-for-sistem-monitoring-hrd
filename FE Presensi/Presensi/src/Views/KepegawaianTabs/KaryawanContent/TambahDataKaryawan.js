import React, { useEffect } from "react";
import { MenuItem, Select,  RadioGroup, Radio, FormControlLabel, makeStyles, LinearProgress, CircularProgress, withStyles, IconButton, InputAdornment, TextField,  Button, TablePagination, Chip, Paper,  Typography, Grid,  SnackbarContent, Snackbar, TextareaAutosize } from "@material-ui/core";
import clsx from 'clsx';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import TodayIcon from '@material-ui/icons/Today';
import {
    MuiPickersUtilsProvider,
    DatePicker
} from '@material-ui/pickers';
import Swal from 'sweetalert2';
import * as apiDataReligion from "../../../Services/apiDataReligion";
import * as apiDataStatus from "../../../Services/apiDataStatus";
import * as apiDataKaryawan from "../../../Services/apiDataKaryawan";
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { amber, green } from '@material-ui/core/colors';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import ImageUploader from 'react-images-upload';
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
let timeoutPromise;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(0.5),
    },
    filterSelect: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    filterInput: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    toolbar: {
        borderBottom: "1px solid #e0e0e0",
        height: (h * 5 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(4, 0),
    },
    toolbarFoot: {
        borderTop: "1px solid #e0e0e0",
        height: (h * 2 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(3, 0),
        marginTop: 5 + "pt",
    },
    searchButton: {
        height: (h * 2.5 / 100) + "px",
        width: (h * 2.5 / 100) + "px",
    },
    rotateHalf: {
        transform: "rotateX(180deg)",
    },
    tableRoot: {
        margin: theme.spacing(2, 0),
    },
    table: {
        minWidth: 650,
    },
    drawerModal: {
        padding: theme.spacing(0),
        overflowX: "hidden",
        minWidth: 500 + "px",
    },
    pageTitle: {
        marginLeft: theme.spacing(5),
        fontSize: 18 + "pt",
    },
    modalContent: {
        padding: theme.spacing(0, 5),
        paddingBottom: theme.spacing(10),
        marginTop: 10 + "pt",
    },
    buttonEdit: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#ffffff",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonDelete: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#FFFFFF",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonContainer: {
        width: 115 + "pt",
        marginLeft: 50 + "%",
        transform: "translate(-50%, 0)",
    },
    buttonControlContainer: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(5),
        width: "auto"
    },
    tableHeader: {
        borderBottom: "2px solid #e0e0e0",
        "& th": {
            fontSize: 11 + "pt",
            fontWeight: "bold",
        },
    },
    buttonControl: {
        fontSize: 8.75 + "pt",
        width: theme.spacing(13),
        padding: theme.spacing(1, 3),
    },
    chipAvatar: {
        height: 42.5 + "px", width: 42.5 + "px",
    },
    textField: {
        width: 100 + "%",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    textArea: {
        width: 100 + "%",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        margin: theme.spacing(0),
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
    },
}));

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 5000
});



function TambahDataKaryawan(props) {
    const classes = useStyles();

    const calculatedPossibleBirthDate = new Date();
    calculatedPossibleBirthDate.setFullYear(calculatedPossibleBirthDate.getFullYear() - 35, 0, 1);

    let data = {
        "nip": "",
        "status": "",
        "religion": "",
        "name": "",
        "dateOfBirth": calculatedPossibleBirthDate,

        "gender": "",
        "placeOfBirth": "",
        "address": "",

        "phoneNumber": "",
        "postalCode": "",
      
        "picture": "",

    }
    const [formState, setFormState] = React.useState(data);

    const [isSaving, setSaving] = React.useState(false);
    const [isFailSaving, setFailSaving] = React.useState(false);
    const [failSavingMessage, setFailSavingMessage] = React.useState("");
    function actionSimpan() {
        var allClear = true;
        setSaving(true);

        Object.entries(formState).forEach((entry) => {
            if ((entry[1] === "" || entry[1] === undefined || entry[1] === null) && entry[0] !== "placeOfBirth" && entry[0] !== "phoneNumber" && entry[0] !== "name" && entry[0] !== "nip" && entry[0] !== "status" && entry[0] !== "religion" && entry[0] !== "dateOfBirth" && entry[0] !== "address") {
                allClear = false;
            }
        });
        if (!allClear) {
            setSaving(false);
            setFailSaving(true);
            let timeout = setTimeout(() => {
                setFailSaving(false);
            }, 5000);
            timeoutPromise = timeout;
            setFailSavingMessage("Input Bertanda * Tidak Boleh Kosong!");

            return;
        }

        

        var errorStatus = false;
        let postFormState = {
        "nip": formState.nip,
        "status": {statusId : formState.status},
        "religion": {religionId : formState.religion},
        "name": formState.name,
        "dateOfBirth": formState.dateOfBirth,

        "gender": formState.gender,
        "placeOfBirth": formState.placeOfBirth,
        "address": formState.address,

        "phoneNumber": formState.phoneNumber,
        "postalCode": formState.postalCode,
      
        "picture": formState.picture,

        }
        console.log("daraaa :" + JSON.stringify(postFormState));
        apiDataKaryawan.postData(postFormState).then((response) => {
            
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                Toast.fire({
                    type: "success",
                    title: "Data Berhasil Ditambah!"
                });
                props.onCancel();
            } else {
                
                setFailSaving(true);
                let timeout = setTimeout(() => {
                    setFailSaving(false);
                }, 2000);
                timeoutPromise = timeout;
                setFailSavingMessage("Data Gagal Disimpan!");
            }
        });
    }

    function actionReset() {
        setFormState(data);
    }

    const handleTextInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleNumberChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value.replace(/[^0-9]/g, '') });
    };

    const [imageDataUrl, setImageDataUrl] = React.useState("");
    const [imageArrayBuffer, setImageArrayBuffer] = React.useState([]);

    
    function handleImageInputChange(event) {
        var file = event.target.files[0];
        //var filename=""; filename=file.name;
        var filesize = (file.size / 1024).toFixed(4); // KB

       

        if (filesize <= 300) {
            setUploadError([false, ""]);
            //setFormState({...formState, "image": {file}});
            //setFormState({ ...formState, "image": { file }, "imagePreview": reader.result });

            var reader1 = new FileReader();
            reader1.onloadend = () => {
                setFormState({ ...formState,  "picture": reader1.result});
            };
            reader1.readAsDataURL(file);
            var reader2 = new FileReader();
            reader2.onloadend = () => {
                var arrayBuffer = reader2.result, arrayUint8 = new Uint8Array(arrayBuffer);
                setImageArrayBuffer(Array.from(arrayUint8));
            };
            reader2.readAsArrayBuffer(file);
        } else {
            setUploadError([true, "Ukuran File Terlalu Besar, Maks. 300KB!"]);
            //*
            let timeout = setTimeout(() => {
                setUploadError([false, ""]);
                timeoutPromise = null;
            }, 5000);
            timeoutPromise = timeout;
            //*/
        }
    };
    // useEffect(() => {
    //     setFormState({ ...formState,  "picture": imageDataUrl});
    //     console.log("picture "+formState.picture);
    // }, [imageDataUrl]);

    const handleDropdownInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleRadioInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleDateChange = (name) => date => {
        var formattedDate = "";
        var year = "", month = "", day = "";

        year += date.getFullYear();
        month += (date.getMonth() + 1); month = ((month.length < 2) ? "0" : "") + month;
        day += date.getDate(); day = ((day.length < 2) ? "0" : "") + day;

        formattedDate = year + "-" + month + "-" + day;

        setFormState({ ...formState, [name]: formattedDate });
    };
    const [mouseHover, setMouseHover] = React.useState(false);
    const [isUploadError, setUploadError] = React.useState([false, ""]);

    const [listStatus, setListStatus] = React.useState({
        "semuastatus": []
    });
    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        apiDataStatus.getData("", "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setListStatus((data) => ({ ...data, "semuastatus": tempData.data.items }));
            }
        });
    }, [setListStatus]);

    const [listReligion, setListReligion] = React.useState({
        "religions": []
    });
    useEffect(() => {
            let tempData = {};
            var errorStatus = false;
            apiDataReligion.getData("", "", "", "").then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListReligion((data) => ({ ...data, "religions": tempData.data.items }));
                }
            });
        
    }, [setListReligion, setFormState]);

    
    //* Cleanup
    useEffect(() => {
        return function cleanUp() {
            if (timeoutPromise) {
                setUploadError([false, ""]);
                clearTimeout(timeoutPromise);
                timeoutPromise = null;
            }
        }
    }, []);
    //*/
    return (
        <div className={classes.modalContent}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={isUploadError[0]}
            >
                <CustomSnackbarContent
                    variant="error"
                    message={isUploadError[1]}
                />
            </Snackbar>
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="stretch"
                spacing={0}
            >

                <Grid item container direction="row" justify="center" alignItems="center" spacing={2} style={{ marginBottom: 25 + "px" }}>
                    <Grid item container direction="column" justify="center" alignItems="center" spacing={2}>
                        <Paper style={{ height: 200, width: 150 }}>
                            <Grid container direction="row" justify="center" alignItems="center" style={{ height: 100 + "%", width: 100 + "%" }} onMouseEnter={() => { setMouseHover(true) }} onMouseLeave={() => { setMouseHover(false) }}>
                                <Grid item>
                                    {
                                        (formState.picture !== "" && formState.picture !== null && !mouseHover) ?
                                            <img style={{ maxHeight: 200, maxWidth: 150 }} src={formState.picture} alt={formState.picture} />
                                            :
                                            <Button
                                                className={clsx(classes.buttonControl, "btn-rounded btn-sm letter-spacing")}
                                                disabled={false}
                                                variant="outlined"
                                                component="label"
                                                htmlFor="photoUpload"
                                            >Upload</Button>
                                    }
                                    <input type="file" id="photoUpload" style={{ display: "none" }} onChange={(event) => { handleImageInputChange(event) }} accept=".jpe, .jpg, .jpeg, .png, .bmp" />
                                </Grid>
                            </Grid>
                        </Paper>
                        <Grid item>
                            <Typography style={{ color: "red", fontSize: "12px" }}>Ukuran maks. file 300KB</Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item container direction="row" justify="center" alignItems="flex-start" spacing={2}>
                    <Grid item md>
                        <Grid item container direction="row" justify="flex-start" spacing={2}>
                            <Grid item><b>Biodata</b></Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Status</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    required
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.status}
                                    onChange={handleDropdownInputChange("status")}>
                                    <MenuItem value="" selected disabled>Position Status</MenuItem>
                                        {(listStatus.semuastatus === null || listStatus.semuastatus.length === 0) ? null :
                                            listStatus.semuastatus.map(row => (
                                                <MenuItem key={row.statusId} value={row.statusId}>{row.statusName}</MenuItem>
                                            ))
                                        }
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>NIP</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="NIP"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.nip}
                                    onChange={handleNumberChange("nip")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                        <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Name</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="Employee Name"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.name}
                                    onChange={handleTextInputChange("name")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Place Of Birth</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="Ex: Bandung"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.nama}
                                    onChange={handleTextInputChange("placeOfBirth")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Date Of Birth</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <DatePicker
                                        required
                                        disableToolbar
                                        disableFuture
                                        variant="inline"
                                        format="yyyy-MM-dd"
                                        margin="normal"
                                        id="date-picker-inline"
                                        className={clsx(classes.textField, classes.dense)}
                                        openTo="YEAR"
                                        views={["year", "month", "date"]}
                                        value={formState.dateOfBirth}
                                        onChange={handleDateChange("dateOfBirth")}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position={'end'}>
                                                    <IconButton>
                                                        <TodayIcon />
                                                    </IconButton>
                                                </InputAdornment>)
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item><hr /></Grid>

                    <Grid item md>
                        <Grid item container direction="row" justify="flex-start" spacing={2}>
                            <Grid item><b>&nbsp;</b></Grid>
                        </Grid>
                        
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Gender</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item container direction="column" justify="center" alignItems="flex-start" spacing={2} md>
                                <Grid item>
                                    <RadioGroup aria-label="position" name="position" value={formState.jenisKelamin} onChange={handleRadioInputChange("gender")} row>
                                        <FormControlLabel
                                            value="Male"
                                            control={<Radio color="primary" icon={<RadioButtonUncheckedIcon fontSize="small" />} checkedIcon={<RadioButtonCheckedIcon fontSize="small" />} />}
                                            label="Male"
                                            labelPlacement="end"
                                            style={{ marginBottom: 0 }}
                                        />
                                        <FormControlLabel
                                            value="Female"
                                            control={<Radio color="primary" icon={<RadioButtonUncheckedIcon fontSize="small" />} checkedIcon={<RadioButtonCheckedIcon fontSize="small" />} />}
                                            label="Female"
                                            labelPlacement="end"
                                            style={{ marginBottom: 0 }}
                                        />
                                    </RadioGroup>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Phone Number</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="Phone Number"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.phoneNumber}
                                    onChange={handleNumberChange("phoneNumber")}
                                    inputProps={{
                                        maxLength: 13,
                                    }}
                                    InputProps={{
                                        startAdornment: <InputAdornment position="start" style={{ paddingBottom: 4 + "px" }}>+62</InputAdornment>,
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Religion</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    required
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.religion}
                                    onChange={handleDropdownInputChange("religion")}
                                >
                                    <MenuItem value="" selected disabled>Religion</MenuItem>
                                        {(listReligion.religions === null || listReligion.religions.length === 0) ? null :
                                            listReligion.religions.map(row => (
                                                <MenuItem key={row.religionId} value={row.religionId}>{row.religionName}</MenuItem>
                                            ))
                                        }
                                    </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Address</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                            <TextField
                                    required
                                    id="standard-multiline-flexible"
                                    placeholder="Address"
                                    className={clsx(classes.textField)}
                                    margin="dense"
                                    rowsMax="5"
                                    style={{ margin: 0 }}
                                    value={formState.address}
                                    onChange={handleTextInputChange("address")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={4} className="vertical-center-text"><span style={{ color: "red" }}> </span>Postal Code</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="Postal Code"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.postalCode}
                                    onChange={handleNumberChange("postalCode")}
                                ></TextField>
                            </Grid>
                        </Grid>
                    

                <Grid item container direction="row" justify="flex-start" alignItems="flex-start" spacing={1} className={clsx(classes.buttonControlContainer)}>
                    {(!isSaving && !isFailSaving) ?
                        <React.Fragment>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-green text-white btn-sm letter-spacing ")}
                                    onClick={actionSimpan}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >simpan</Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-yellow text-white btn-sm letter-spacing")}
                                    onClick={actionReset}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >reset</Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-red text-white btn-sm letter-spacing")}
                                    onClick={props.onCancel}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >batal</Button>
                            </Grid>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Snackbar
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                open={isSaving || isFailSaving}
                            >
                                <CustomSnackbarContent
                                    variant={(isSaving) ? "info-process" : (isFailSaving) ? "error" : null}
                                    message={(isSaving) ? "Submitting..." : (isFailSaving) ? failSavingMessage : null}
                                />
                            </Snackbar>
                        </React.Fragment>
                    }
                </Grid>
            </Grid>
            </Grid>
            </Grid>
        </div >
    );
}
const CustomCircularProgress = withStyles({
    root: {
        color: '#517296',
    },
})(CircularProgress);
const CustomLinearProgress = withStyles({
    colorPrimary: {
        height: 4 + "px",
        borderRadius: "0px 0px 4px 4px",
        backgroundColor: '#d3d3d3',
    },
    barColorPrimary: {
        borderRadius: 25 + "px",
        backgroundColor: '#517296',
    },
})(LinearProgress);

// SnackBar
const LoadingIcon = withStyles(theme => ({
    root: {
        color: amber[100],
    },
}))(CircularProgress);
/*
LoadingIcon.defaultProps={
    size: 25,
    thickness: 4,
}
//*/
const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
    "info-process": WarningIcon,
};
const useSnackBarStyles = makeStyles(theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    "info-process": {
        backgroundColor: amber[800],
        color: theme.palette.common.white,
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));
function CustomSnackbarContent(props) {
    const classes = useSnackBarStyles();
    const { className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={clsx(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.iconVariant)} />
                    {message}
                    {(variant === "info-process") ? <LoadingIcon style={{ position: "absolute", right: 20 + "px" }} size={25} thickness={4} /> : null}
                </span>
            }
            {...other}
        />
    );
}
export default TambahDataKaryawan;

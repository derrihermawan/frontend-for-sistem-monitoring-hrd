
import * as apiDataKaryawan from "../../../Services/apiDataKaryawan";
import Moment from 'react-moment';
import { makeStyles} from "@material-ui/core";
import React, { useEffect } from "react";
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(0.5),
    },
    filterSelect: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    filterInput: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    toolbar: {
        borderBottom: "1px solid #e0e0e0",
        height: (h * 5 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(4, 0),
    },
    toolbarFoot: {
        borderTop: "1px solid #e0e0e0",
        height: (h * 2 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(3, 0),
        marginTop: 5 + "pt",
    },
    searchButton: {
        height: (h * 2.5 / 100) + "px",
        width: (h * 2.5 / 100) + "px",
    },
    rotateHalf: {
        transform: "rotateX(180deg)",
    },
    tableRoot: {
        margin: theme.spacing(2, 0),
    },
    table: {
        minWidth: 650,
    },
    drawerModal: {
        padding: theme.spacing(0),
        overflowX: "hidden",
        minWidth: 500 + "px",
    },
    pageTitle: {
        marginLeft: theme.spacing(5),
        fontSize: 18 + "pt",
    },
    modalContent: {
        padding: theme.spacing(0, 5),
        paddingBottom: theme.spacing(10),
        marginTop: 10 + "pt",
    },
    buttonEdit: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#ffffff",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonDelete: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#FFFFFF",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonContainer: {
        width: 115 + "pt",
        marginLeft: 50 + "%",
        transform: "translate(-50%, 0)",
    },
    buttonControlContainer: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(5),
        width: "auto"
    },
    tableHeader: {
        borderBottom: "2px solid #e0e0e0",
        "& th": {
            fontSize: 11 + "pt",
            fontWeight: "bold",
        },
    },
    buttonControl: {
        fontSize: 8.75 + "pt",
        width: theme.spacing(13),
        padding: theme.spacing(1, 3),
    },
    chipAvatar: {
        height: 42.5 + "px", width: 42.5 + "px",
    },
    textField: {
        width: 100 + "%",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        margin: theme.spacing(0),
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
    },
}));

function DetailKaryawan(props) {
    const classes = useStyles();

    const [details, setDetails] = React.useState({});
    Moment.globalFormat = 'YYYY MMM DD'
    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        
        apiDataKaryawan.getData("nip=" + (props.row.nip), "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setDetails((details) =>
                    ({
                        ...details,
                        
                        nip: tempData.data.items[0].nip,
                        name: tempData.data.items[0].name,
                        status: tempData.data.items[0].status,
                        religion: tempData.data.items[0].religion,
                        gender: tempData.data.items[0].gender,
                        placeOfBirth: tempData.data.items[0].placeOfBirth,
                        dateOfBirth: tempData.data.items[0].dateOfBirth,
                        address: tempData.data.items[0].address,
                        phoneNumber: tempData.data.items[0].phoneNumber,
                        postalCode: tempData.data.items[0].postalCode,
                        
                    }));

            }
        });

        
    }, [props.id]);
    return (
        <div className={classes.modalContent}>
            <div className="row">
                <div className="col-auto mt-2">
                <span className="font-weight-bold">NIP</span>
                </div>
                <div className="col">
                    <div className="col-12 mt-2">
                        <span className="font-weight-bold">{details.nip}</span>
                    </div>
                </div>
            </div>
            
            
            <div className="row mt-3">
                <div className="col-5">
                    <span className="text-secondary-custom">Name</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.name}</span>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-5">
                    <span className="text-secondary-custom">Status</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.status}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Tempat Tanggal Lahir</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.placeOfBirth}, <Moment>{details.dateOfBirth}</Moment> </span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Jenis Kelamin</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.gender}</span>
                </div>
            </div>
            
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Agama</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.religion}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Alamat</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.address} {details.postalCode}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">No Telepon</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.phoneNumber}</span>
                </div>
            </div>
        </div>
    );
}
export default DetailKaryawan;
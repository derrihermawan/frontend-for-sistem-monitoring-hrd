import 'date-fns';
import React, { useEffect } from "react";
//import MaskedInput from 'react-text-mask'
import PropTypes from 'prop-types';
import clsx from 'clsx';

import * as apiDataPengajarEkskul from "../../../Services/apiDataPengajarEkskul";
import * as optionLists from "../../../Services/optionLists";
import * as apiDaerahIndonesia from "../../../Services/apiDaerahIndonesia_ThirdParty";
import { nationality } from "../../../Components/_Nationality";
import { useTableData } from "../../../Hooks";

import { MenuItem, Select, FormControl, RadioGroup, Radio, FormControlLabel, makeStyles, LinearProgress, CircularProgress, withStyles, IconButton, InputAdornment, TextField, Input, Box, Tooltip, Button, TablePagination, Chip, Paper, Table, TableHead, TableRow, TableBody, TableCell, Drawer, Link, Container, Typography, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Grid, Avatar, SnackbarContent, Snackbar, ListSubheader, Divider } from "@material-ui/core";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    DatePicker
} from '@material-ui/pickers';

import SearchIcon from "@material-ui/icons/Search";
import SortIcon from '@material-ui/icons/Sort';
import AddIcon from '@material-ui/icons/Add';
import TodayIcon from '@material-ui/icons/Today';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import { amber, green } from '@material-ui/core/colors';
import WarningIcon from '@material-ui/icons/Warning';

import Swal from 'sweetalert2';

var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
let timeoutPromise;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(0.5),
    },
    filterSelect: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    filterInput: {
        margin: theme.spacing(0),
        minWidth: (h * 10 / 100) + "pt",
        maxWidth: (h * 15 / 100) + "pt",
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    toolbar: {
        borderBottom: "1px solid #e0e0e0",
        height: (h * 5 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(4, 0),
    },
    toolbarFoot: {
        borderTop: "1px solid #e0e0e0",
        height: (h * 2 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(3, 0),
        marginTop: 5 + "pt",
    },
    searchButton: {
        height: (h * 2.5 / 100) + "px",
        width: (h * 2.5 / 100) + "px",
    },
    rotateHalf: {
        transform: "rotateX(180deg)",
    },
    tableRoot: {
        margin: theme.spacing(2, 0),
    },
    table: {
        minWidth: 650,
    },
    drawerModal: {
        padding: theme.spacing(0),
        overflowX: "hidden",
        minWidth: 500 + "px",
    },
    pageTitle: {
        marginLeft: theme.spacing(5),
        fontSize: 18 + "pt",
    },
    modalContent: {
        padding: theme.spacing(0, 5),
        paddingBottom: theme.spacing(10),
        marginTop: 10 + "pt",
    },
    buttonEdit: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#ffffff",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonDelete: {
        margin: 1 + "pt",
        borderRadius: 500 + "pt",
        fontSize: 10 + "pt",
        border: 0,
        color: "#FFFFFF",
        minWidth: 55 + "pt",
        minHeight: 25 + "pt",
    },
    buttonContainer: {
        width: 115 + "pt",
        marginLeft: 50 + "%",
        transform: "translate(-50%, 0)",
    },
    buttonControlContainer: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(5),
        width: "auto"
    },
    tableHeader: {
        borderBottom: "2px solid #e0e0e0",
        "& th": {
            fontSize: 11 + "pt",
            fontWeight: "bold",
        },
    },
    buttonControl: {
        fontSize: 8.75 + "pt",
        width: theme.spacing(13),
        padding: theme.spacing(1, 3),
    },
    chipAvatar: {
        height: 42.5 + "px", width: 42.5 + "px",
    },
    textField: {
        width: 100 + "%",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        margin: theme.spacing(0),
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
    },
}));

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 5000
});

// Components
function DetailData(props) {
    const classes = useStyles();

    const [details, setDetails] = React.useState({});
    const [ekskul, setEkskul] = React.useState({ "items": [] });

    useEffect(() => {
        let tempData = {};
        var errorStatus = false;

        apiDataPengajarEkskul.getData("idPengajarEskul=" + (props.id), "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setDetails((details) =>
                    ({
                        ...details,
                        rowNumber: tempData.data.items[0].rowNumber,
                        nik: tempData.data.items[0].nik,
                        idPengajarEskul: tempData.data.items[0].idPengajarEskul,
                        namaPengajar: tempData.data.items[0].namaPengajar,
                        ekstrakulikuler: tempData.data.items[0].ekstrakulikuler,
                        tempatTanggalLahir: tempData.data.items[0].tempatTanggalLahir,
                        noHp: tempData.data.items[0].noHp,
                        image: tempData.data.items[0].image,
                        agama: tempData.data.items[0].agama,
                        jenisKelamin: tempData.data.items[0].jenisKelamin,
                        alamatJalan: tempData.data.items[0].alamatJalan,
                        kecamatan: tempData.data.items[0].kecamatan,
                        golDarah: tempData.data.items[0].golDarah,
                        kelurahan: tempData.data.items[0].kelurahan,
                        provinsi: tempData.data.items[0].provinsi,
                        kotaKabupaten: tempData.data.items[0].kotaKabupaten,
                        kodePos: tempData.data.items[0].kodePos,
                        kewarnegaraan: tempData.data.items[0].kewarnegaraan,
                    }));

            }
        });

        apiDataPengajarEkskul.getDetails("", "", "", "", (props.id)).then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setEkskul((ekskul) =>
                    ({
                        ...ekskul,
                        "items": tempData.data.items
                    }));
            }
        });
    }, [props.id]);

    return (
        <div className={classes.modalContent}>
            <div className="row">
                <div className="col-auto">
                    <img src={details.image} style={{ maxHeight: "200px", maxWidth: "150px" }} alt="" />
                </div>
                <div className="col">
                    <div className="col-12 mt-2">
                        <span className="font-weight-bold">{details.nik}</span>
                    </div>
                    <div className="col-12 mt-2">
                        <span className="font-weight-bold">{details.namaPengajar}</span>
                    </div>
                    <div className="col-12 mt-2">
                        <span className="font-weight-bold">{details.noHp}</span>
                    </div>
                </div>
            </div>
            <Grid item container direction="column" justify="flex-start" alignItems="stretch" spacing={2} style={{ marginTop: 5 + "pt" }}>
                <Grid item>Daftar Ekstrakurikuler</Grid>
                {ekskul.items.map(row => (
                    <Grid key={row.idPengajarEskul} item>
                        <Chip label={row.namaEskul} variant="outlined" />
                    </Grid>
                ))}
            </Grid>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Tempat Tanggal Lahir</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.tempatTanggalLahir}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Jenis Kelamin</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.jenisKelamin}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Alamat</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.alamatJalan}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Kecamatan</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.kecamatan}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Kelurahan</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.kelurahan}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Provinsi</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.provinsi}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Kota/Kabupaten</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.kotaKabupaten}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Kode Pos</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.kodePos}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Agama</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-auto no-margin">
                    <span className="text-secondary-custom">{details.agama}</span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Kewarganegaraan</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">
                        {nationality[details.kewarnegaraan]}
                    </span>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-5">
                    <span className="text-secondary-custom">Golongan Darah</span>
                </div>
                <div className="col-1">
                    <span className="text-secondary-custom">:</span>
                </div>
                <div className="col-6 no-margin">
                    <span className="text-secondary-custom">{details.golDarah}</span>
                </div>
            </div>
        </div>
    );
}
function EditData(props) {
    const classes = useStyles();

    const calculatedPossibleBirthDate = new Date();
    calculatedPossibleBirthDate.setFullYear(calculatedPossibleBirthDate.getFullYear() - 35, 0, 1);

    let data = React.useRef({
        "nik": "",
        "namaPengajar": "",
        "tempatLahir": "",
        "tanggalLahir": calculatedPossibleBirthDate,
        "jenisKelamin": "L",
        "noHp": "",
        "alamatJalan": "",
        "kelurahan": "",
        "kecamatan": "",
        "kotaKabupaten": "",
        "provinsi": "",
        "kodePos": "",
        "agama": "islam",
        "golDarah": "A",
        "kewarnegaraan": "ID",
        // "imageType":"",
        // "imageByte" : ""
    })
    const [formState, setFormState] = React.useState(data.current);

    const [isSaving, setSaving] = React.useState(false);
    const [isFailSaving, setFailSaving] = React.useState(false);
    const [failSavingMessage, setFailSavingMessage] = React.useState("");
    function actionSimpan() {
        var allClear = true;
        setSaving(true);

        Object.entries(formState).forEach((entry) => {
            if ((entry[1] === "" || entry[1] === undefined || entry[1] === null) && entry[0] !== "image") {
                allClear = false;
            }
        });
        if (!allClear) {
            setSaving(false);
            setFailSaving(true);
            let timeout = setTimeout(() => {
                setFailSaving(false);
            }, 5000);
            timeoutPromise = timeout;
            setFailSavingMessage("Input Bertanda * Tidak Boleh Kosong!");

            return;
        }

        var errorStatus = false;
        apiDataPengajarEkskul.putData(formState, props.id).then((response) => {

        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                Toast.fire({
                    type: "success",
                    title: "Data Berhasil Ditambah!"
                });
                props.onCancel();
            } else {
                // Toast.fire({
                //     type: "error",
                //     title: "Data Gagal Ditambah!"
                // });
                setFailSaving(true);
                let timeout = setTimeout(() => {
                    setFailSaving(false);
                }, 2000);
                timeoutPromise = timeout;
                setFailSavingMessage("Data Gagal Disimpan!");
            }
        });
    }

    function actionReset() {
        setFormState(() =>
            ({
                ...data.current
            })
        );
    }

    const handleTextInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleNumberChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value.replace(/[^0-9]/g, '') });
    };

    const handleImageInputChange = () => event => {
        var file = event.target.files[0];
        //var filename = ""; filename = file.name;
        var filesize = (file.size / 1024).toFixed(4); // KB

        if (filesize <= 300) {
            setUploadError([false, ""]);

            var reader = new FileReader();
            reader.onloadend = () => {
                setFormState({ ...formState, "image": reader.result });
            };
            reader.readAsDataURL(file);
        } else {
            setUploadError([true, "Ukuran File Terlalu Besar, Maks. 300KB!"]);
            //*
            let timeout = setTimeout(() => {
                setUploadError([false, ""]);
                timeoutPromise = null;
            }, 5000);
            timeoutPromise = timeout;
            //*/
        }
    };
    const handleDropdownInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleRadioInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleDateChange = (name) => date => {
        var formattedDate = "";
        var year = "", month = "", day = "";

        year += date.getFullYear();
        month += (date.getMonth() + 1); month = ((month.length < 2) ? "0" : "") + month;
        day += date.getDate(); day = ((day.length < 2) ? "0" : "") + day;

        formattedDate = year + "-" + month + "-" + day;

        setFormState({ ...formState, [name]: formattedDate });
    };
    const [mouseHover, setMouseHover] = React.useState(false);
    const [isUploadError, setUploadError] = React.useState([false, ""]);

    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        apiDataPengajarEkskul.getData("idPengajarEskul=" + (props.id), "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                data.current = tempData.data.items[0];
                setFormState(() =>
                    ({
                        ...data.current
                    })
                );
            }
        });
    }, [props.id]);

    //API alamat
    const [listProvinsi, setListProvinsi] = React.useState({
        "semuaprovinsi": []
    });
    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        apiDaerahIndonesia.getProvinsi().then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setListProvinsi((data) => ({ ...data, "semuaprovinsi": tempData.data.semuaprovinsi }));
            }
        });
    }, [setListProvinsi]);

    const [listKabupaten, setListKabupaten] = React.useState({
        "kabupatens": []
    });
    useEffect(() => {
        if (formState.provinsi !== null && formState.provinsi !== undefined && formState.provinsi !== ""){

            let tempData = {};
            var errorStatus = false;
            apiDaerahIndonesia.getKabupaten(formState.provinsi).then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListKabupaten((data) => ({ ...data, "kabupatens": tempData.data.kabupatens }));
                }
            });
        }
    }, [setListKabupaten, setFormState, formState.provinsi]);

    const [listKecamatan, setListKecamatan] = React.useState({
        "kecamatans": []
    });
    useEffect(() => {
        if (formState.kotaKabupaten !== null && formState.kotaKabupaten !== undefined && formState.kotaKabupaten !== ""){

            let tempData = {};
            var errorStatus = false;
            apiDaerahIndonesia.getKecamatan(formState.kotaKabupaten).then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListKecamatan((data) => ({ ...data, "kecamatans": tempData.data.kecamatans }));
                }
            });
        }
    }, [setListKecamatan, setFormState, formState.kotaKabupaten]);

    const [listDesa, setListDesa] = React.useState({
        "desas": []
    });
    useEffect(() => {
        if (formState.kecamatan !== null && formState.kecamatan !== undefined && formState.kecamatan !== ""){

            let tempData = {};
            var errorStatus = false;
            apiDaerahIndonesia.getDesa(formState.kecamatan).then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListDesa((data) => ({ ...data, "desas": tempData.data.desas }));
                }
            });
        }
    }, [setListDesa, setFormState, formState.kecamatan]);


    //* Cleanup
    useEffect(() => {
        return function cleanUp() {
            if (timeoutPromise) {
                setUploadError([false, ""]);
                clearTimeout(timeoutPromise);
                timeoutPromise = null;
            }
        }
    }, []);
    //*/

    return (
        <div className={classes.modalContent}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={isUploadError[0]}
            >
                <CustomSnackbarContent
                    variant="error"
                    message={isUploadError[1]}
                />
            </Snackbar>
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="stretch"
                spacing={0}
            >

                <Grid item container direction="row" justify="center" alignItems="center" spacing={2} style={{ marginBottom: 25 + "px" }}>
                    <Grid item container direction="column" justify="center" alignItems="center" spacing={2}>
                        <Paper style={{ height: 200, width: 150 }}>
                            <Grid container direction="row" justify="center" alignItems="center" style={{ height: 100 + "%", width: 100 + "%" }} onMouseEnter={() => { setMouseHover(true) }} onMouseLeave={() => { setMouseHover(false) }}>
                                <Grid item>
                                    {
                                        (formState.image !== "" && formState.image !== null && !mouseHover) ?
                                            <img style={{ maxHeight: "200px", maxWidth: "150px" }} src={formState.image} alt="" />
                                            :
                                            <Button
                                                className={clsx(classes.buttonControl, "btn-rounded btn-sm letter-spacing")}
                                                disabled={false}
                                                variant="outlined"
                                                component="label"
                                                htmlFor="photoUpload"
                                            >unggah</Button>
                                    }
                                    <input type="file" id="photoUpload" style={{ display: "none" }} onChange={handleImageInputChange("image")} accept=".jpe, .jpg, .jpeg, .png, .bmp" />
                                </Grid>
                            </Grid>
                        </Paper>
                        <Grid item>
                            <Typography style={{ color: "red", fontSize: "12px" }}>Ukuran maks. file 300KB</Typography>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item container direction="row" justify="center" alignItems="flex-start" spacing={2}>
                    <Grid item md>
                        <Grid item container direction="row" justify="flex-start" spacing={2}>
                            <Grid item><b>Biodata</b></Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>NIK</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="nik"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.nik}
                                    onChange={handleNumberChange("nik")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Nama</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="nama pengajar"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.namaPengajar}
                                    onChange={handleTextInputChange("namaPengajar")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Tempat Lahir</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="tempat lahir"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.tempatLahir}
                                    onChange={handleTextInputChange("tempatLahir")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Tanggal Lahir</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <DatePicker
                                        required
                                        disableToolbar
                                        disableFuture
                                        variant="inline"
                                        format="yyyy-MM-dd"
                                        margin="normal"
                                        id="date-picker-inline"
                                        className={clsx(classes.textField, classes.dense)}
                                        openTo="YEAR"
                                        views={["year", "month", "date"]}
                                        value={formState.tanggalLahir}
                                        onChange={handleDateChange("tanggalLahir")}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position={'end'}>
                                                    <IconButton>
                                                        <TodayIcon />
                                                    </IconButton>
                                                </InputAdornment>)
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Jenis Kelamin</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item container direction="column" justify="center" alignItems="flex-start" spacing={2} md>
                                <Grid item>
                                    <RadioGroup aria-label="position" name="position" value={formState.jenisKelamin} onChange={handleRadioInputChange("jenisKelamin")} row>
                                        <FormControlLabel
                                            value="L"
                                            control={<Radio color="primary" icon={<RadioButtonUncheckedIcon fontSize="small" />} checkedIcon={<RadioButtonCheckedIcon fontSize="small" />} />}
                                            label="L"
                                            labelPlacement="end"
                                            style={{ marginBottom: 0 }}
                                        />
                                        <FormControlLabel
                                            value="P"
                                            control={<Radio color="primary" icon={<RadioButtonUncheckedIcon fontSize="small" />} checkedIcon={<RadioButtonCheckedIcon fontSize="small" />} />}
                                            label="P"
                                            labelPlacement="end"
                                            style={{ marginBottom: 0 }}
                                        />
                                    </RadioGroup>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>No. HP</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="nomor handphone"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.noHp}
                                    onChange={handleNumberChange("noHp")}
                                    inputProps={{
                                        maxLength: 13,
                                    }}
                                    InputProps={{
                                        startAdornment: <InputAdornment position="start" style={{ paddingBottom: 5 + "px" }}>+62</InputAdornment>,
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Agama</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    required
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.agama}
                                    onChange={handleDropdownInputChange("agama")}
                                >
                                    <MenuItem selected disabled>agama</MenuItem>
                                    <MenuItem value="islam">Islam</MenuItem>
                                    <MenuItem value="kristen">Kristen</MenuItem>
                                    <MenuItem value="katholik">Katholik</MenuItem>
                                    <MenuItem value="hindu">Hindu</MenuItem>
                                    <MenuItem value="buddha">Buddha</MenuItem>
                                    <MenuItem value="konghuchu">Konghuchu</MenuItem>
                                    <MenuItem value="lain">Lainnya</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kewarganegaraan</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    required
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.kewarnegaraan}
                                    onChange={handleDropdownInputChange("kewarnegaraan")}
                                >
                                    <MenuItem selected disabled>kewarganegaraan</MenuItem>
                                    <MenuItem value="AF">Afghanistan</MenuItem>
                                    <MenuItem value="AX">Åland Islands</MenuItem>
                                    <MenuItem value="AL">Albania</MenuItem>
                                    <MenuItem value="DZ">Algeria</MenuItem>
                                    <MenuItem value="AS">American Samoa</MenuItem>
                                    <MenuItem value="AD">Andorra</MenuItem>
                                    <MenuItem value="AO">Angola</MenuItem>
                                    <MenuItem value="AI">Anguilla</MenuItem>
                                    <MenuItem value="AQ">Antarctica</MenuItem>
                                    <MenuItem value="AG">Antigua and Barbuda</MenuItem>
                                    <MenuItem value="AR">Argentina</MenuItem>
                                    <MenuItem value="AM">Armenia</MenuItem>
                                    <MenuItem value="AW">Aruba</MenuItem>
                                    <MenuItem value="AU">Australia</MenuItem>
                                    <MenuItem value="AT">Austria</MenuItem>
                                    <MenuItem value="AZ">Azerbaijan</MenuItem>
                                    <MenuItem value="BS">Bahamas</MenuItem>
                                    <MenuItem value="BH">Bahrain</MenuItem>
                                    <MenuItem value="BD">Bangladesh</MenuItem>
                                    <MenuItem value="BB">Barbados</MenuItem>
                                    <MenuItem value="BY">Belarus</MenuItem>
                                    <MenuItem value="BE">Belgium</MenuItem>
                                    <MenuItem value="BZ">Belize</MenuItem>
                                    <MenuItem value="BJ">Benin</MenuItem>
                                    <MenuItem value="BM">Bermuda</MenuItem>
                                    <MenuItem value="BT">Bhutan</MenuItem>
                                    <MenuItem value="BO">Bolivia, Plurinational State of</MenuItem>
                                    <MenuItem value="BQ">Bonaire, Sint Eustatius and Saba</MenuItem>
                                    <MenuItem value="BA">Bosnia and Herzegovina</MenuItem>
                                    <MenuItem value="BW">Botswana</MenuItem>
                                    <MenuItem value="BV">Bouvet Island</MenuItem>
                                    <MenuItem value="BR">Brazil</MenuItem>
                                    <MenuItem value="IO">British Indian Ocean Territory</MenuItem>
                                    <MenuItem value="BN">Brunei Darussalam</MenuItem>
                                    <MenuItem value="BG">Bulgaria</MenuItem>
                                    <MenuItem value="BF">Burkina Faso</MenuItem>
                                    <MenuItem value="BI">Burundi</MenuItem>
                                    <MenuItem value="KH">Cambodia</MenuItem>
                                    <MenuItem value="CM">Cameroon</MenuItem>
                                    <MenuItem value="CA">Canada</MenuItem>
                                    <MenuItem value="CV">Cape Verde</MenuItem>
                                    <MenuItem value="KY">Cayman Islands</MenuItem>
                                    <MenuItem value="CF">Central African Republic</MenuItem>
                                    <MenuItem value="TD">Chad</MenuItem>
                                    <MenuItem value="CL">Chile</MenuItem>
                                    <MenuItem value="CN">China</MenuItem>
                                    <MenuItem value="CX">Christmas Island</MenuItem>
                                    <MenuItem value="CC">Cocos (Keeling) Islands</MenuItem>
                                    <MenuItem value="CO">Colombia</MenuItem>
                                    <MenuItem value="KM">Comoros</MenuItem>
                                    <MenuItem value="CG">Congo</MenuItem>
                                    <MenuItem value="CD">Congo, the Democratic Republic of the</MenuItem>
                                    <MenuItem value="CK">Cook Islands</MenuItem>
                                    <MenuItem value="CR">Costa Rica</MenuItem>
                                    <MenuItem value="CI">Côte d'Ivoire</MenuItem>
                                    <MenuItem value="HR">Croatia</MenuItem>
                                    <MenuItem value="CU">Cuba</MenuItem>
                                    <MenuItem value="CW">Curaçao</MenuItem>
                                    <MenuItem value="CY">Cyprus</MenuItem>
                                    <MenuItem value="CZ">Czech Republic</MenuItem>
                                    <MenuItem value="DK">Denmark</MenuItem>
                                    <MenuItem value="DJ">Djibouti</MenuItem>
                                    <MenuItem value="DM">Dominica</MenuItem>
                                    <MenuItem value="DO">Dominican Republic</MenuItem>
                                    <MenuItem value="EC">Ecuador</MenuItem>
                                    <MenuItem value="EG">Egypt</MenuItem>
                                    <MenuItem value="SV">El Salvador</MenuItem>
                                    <MenuItem value="GQ">Equatorial Guinea</MenuItem>
                                    <MenuItem value="ER">Eritrea</MenuItem>
                                    <MenuItem value="EE">Estonia</MenuItem>
                                    <MenuItem value="ET">Ethiopia</MenuItem>
                                    <MenuItem value="FK">Falkland Islands (Malvinas)</MenuItem>
                                    <MenuItem value="FO">Faroe Islands</MenuItem>
                                    <MenuItem value="FJ">Fiji</MenuItem>
                                    <MenuItem value="FI">Finland</MenuItem>
                                    <MenuItem value="FR">France</MenuItem>
                                    <MenuItem value="GF">French Guiana</MenuItem>
                                    <MenuItem value="PF">French Polynesia</MenuItem>
                                    <MenuItem value="TF">French Southern Territories</MenuItem>
                                    <MenuItem value="GA">Gabon</MenuItem>
                                    <MenuItem value="GM">Gambia</MenuItem>
                                    <MenuItem value="GE">Georgia</MenuItem>
                                    <MenuItem value="DE">Germany</MenuItem>
                                    <MenuItem value="GH">Ghana</MenuItem>
                                    <MenuItem value="GI">Gibraltar</MenuItem>
                                    <MenuItem value="GR">Greece</MenuItem>
                                    <MenuItem value="GL">Greenland</MenuItem>
                                    <MenuItem value="GD">Grenada</MenuItem>
                                    <MenuItem value="GP">Guadeloupe</MenuItem>
                                    <MenuItem value="GU">Guam</MenuItem>
                                    <MenuItem value="GT">Guatemala</MenuItem>
                                    <MenuItem value="GG">Guernsey</MenuItem>
                                    <MenuItem value="GN">Guinea</MenuItem>
                                    <MenuItem value="GW">Guinea-Bissau</MenuItem>
                                    <MenuItem value="GY">Guyana</MenuItem>
                                    <MenuItem value="HT">Haiti</MenuItem>
                                    <MenuItem value="HM">Heard Island and McDonald Islands</MenuItem>
                                    <MenuItem value="VA">Holy See (Vatican City State)</MenuItem>
                                    <MenuItem value="HN">Honduras</MenuItem>
                                    <MenuItem value="HK">Hong Kong</MenuItem>
                                    <MenuItem value="HU">Hungary</MenuItem>
                                    <MenuItem value="IS">Iceland</MenuItem>
                                    <MenuItem value="IN">India</MenuItem>
                                    <MenuItem value="ID">Indonesia</MenuItem>
                                    <MenuItem value="IR">Iran, Islamic Republic of</MenuItem>
                                    <MenuItem value="IQ">Iraq</MenuItem>
                                    <MenuItem value="IE">Ireland</MenuItem>
                                    <MenuItem value="IM">Isle of Man</MenuItem>
                                    <MenuItem value="IL">Israel</MenuItem>
                                    <MenuItem value="IT">Italy</MenuItem>
                                    <MenuItem value="JM">Jamaica</MenuItem>
                                    <MenuItem value="JP">Japan</MenuItem>
                                    <MenuItem value="JE">Jersey</MenuItem>
                                    <MenuItem value="JO">Jordan</MenuItem>
                                    <MenuItem value="KZ">Kazakhstan</MenuItem>
                                    <MenuItem value="KE">Kenya</MenuItem>
                                    <MenuItem value="KI">Kiribati</MenuItem>
                                    <MenuItem value="KP">Korea, Democratic People's Republic of</MenuItem>
                                    <MenuItem value="KR">Korea, Republic of</MenuItem>
                                    <MenuItem value="KW">Kuwait</MenuItem>
                                    <MenuItem value="KG">Kyrgyzstan</MenuItem>
                                    <MenuItem value="LA">Lao People's Democratic Republic</MenuItem>
                                    <MenuItem value="LV">Latvia</MenuItem>
                                    <MenuItem value="LB">Lebanon</MenuItem>
                                    <MenuItem value="LS">Lesotho</MenuItem>
                                    <MenuItem value="LR">Liberia</MenuItem>
                                    <MenuItem value="LY">Libya</MenuItem>
                                    <MenuItem value="LI">Liechtenstein</MenuItem>
                                    <MenuItem value="LT">Lithuania</MenuItem>
                                    <MenuItem value="LU">Luxembourg</MenuItem>
                                    <MenuItem value="MO">Macao</MenuItem>
                                    <MenuItem value="MK">Macedonia, the former Yugoslav Republic of</MenuItem>
                                    <MenuItem value="MG">Madagascar</MenuItem>
                                    <MenuItem value="MW">Malawi</MenuItem>
                                    <MenuItem value="MY">Malaysia</MenuItem>
                                    <MenuItem value="MV">Maldives</MenuItem>
                                    <MenuItem value="ML">Mali</MenuItem>
                                    <MenuItem value="MT">Malta</MenuItem>
                                    <MenuItem value="MH">Marshall Islands</MenuItem>
                                    <MenuItem value="MQ">Martinique</MenuItem>
                                    <MenuItem value="MR">Mauritania</MenuItem>
                                    <MenuItem value="MU">Mauritius</MenuItem>
                                    <MenuItem value="YT">Mayotte</MenuItem>
                                    <MenuItem value="MX">Mexico</MenuItem>
                                    <MenuItem value="FM">Micronesia, Federated States of</MenuItem>
                                    <MenuItem value="MD">Moldova, Republic of</MenuItem>
                                    <MenuItem value="MC">Monaco</MenuItem>
                                    <MenuItem value="MN">Mongolia</MenuItem>
                                    <MenuItem value="ME">Montenegro</MenuItem>
                                    <MenuItem value="MS">Montserrat</MenuItem>
                                    <MenuItem value="MA">Morocco</MenuItem>
                                    <MenuItem value="MZ">Mozambique</MenuItem>
                                    <MenuItem value="MM">Myanmar</MenuItem>
                                    <MenuItem value="NA">Namibia</MenuItem>
                                    <MenuItem value="NR">Nauru</MenuItem>
                                    <MenuItem value="NP">Nepal</MenuItem>
                                    <MenuItem value="NL">Netherlands</MenuItem>
                                    <MenuItem value="NC">New Caledonia</MenuItem>
                                    <MenuItem value="NZ">New Zealand</MenuItem>
                                    <MenuItem value="NI">Nicaragua</MenuItem>
                                    <MenuItem value="NE">Niger</MenuItem>
                                    <MenuItem value="NG">Nigeria</MenuItem>
                                    <MenuItem value="NU">Niue</MenuItem>
                                    <MenuItem value="NF">Norfolk Island</MenuItem>
                                    <MenuItem value="MP">Northern Mariana Islands</MenuItem>
                                    <MenuItem value="NO">Norway</MenuItem>
                                    <MenuItem value="OM">Oman</MenuItem>
                                    <MenuItem value="PK">Pakistan</MenuItem>
                                    <MenuItem value="PW">Palau</MenuItem>
                                    <MenuItem value="PS">Palestinian Territory, Occupied</MenuItem>
                                    <MenuItem value="PA">Panama</MenuItem>
                                    <MenuItem value="PG">Papua New Guinea</MenuItem>
                                    <MenuItem value="PY">Paraguay</MenuItem>
                                    <MenuItem value="PE">Peru</MenuItem>
                                    <MenuItem value="PH">Philippines</MenuItem>
                                    <MenuItem value="PN">Pitcairn</MenuItem>
                                    <MenuItem value="PL">Poland</MenuItem>
                                    <MenuItem value="PT">Portugal</MenuItem>
                                    <MenuItem value="PR">Puerto Rico</MenuItem>
                                    <MenuItem value="QA">Qatar</MenuItem>
                                    <MenuItem value="RE">Réunion</MenuItem>
                                    <MenuItem value="RO">Romania</MenuItem>
                                    <MenuItem value="RU">Russian Federation</MenuItem>
                                    <MenuItem value="RW">Rwanda</MenuItem>
                                    <MenuItem value="BL">Saint Barthélemy</MenuItem>
                                    <MenuItem value="SH">Saint Helena, Ascension and Tristan da Cunha</MenuItem>
                                    <MenuItem value="KN">Saint Kitts and Nevis</MenuItem>
                                    <MenuItem value="LC">Saint Lucia</MenuItem>
                                    <MenuItem value="MF">Saint Martin (French part)</MenuItem>
                                    <MenuItem value="PM">Saint Pierre and Miquelon</MenuItem>
                                    <MenuItem value="VC">Saint Vincent and the Grenadines</MenuItem>
                                    <MenuItem value="WS">Samoa</MenuItem>
                                    <MenuItem value="SM">San Marino</MenuItem>
                                    <MenuItem value="ST">Sao Tome and Principe</MenuItem>
                                    <MenuItem value="SA">Saudi Arabia</MenuItem>
                                    <MenuItem value="SN">Senegal</MenuItem>
                                    <MenuItem value="RS">Serbia</MenuItem>
                                    <MenuItem value="SC">Seychelles</MenuItem>
                                    <MenuItem value="SL">Sierra Leone</MenuItem>
                                    <MenuItem value="SG">Singapore</MenuItem>
                                    <MenuItem value="SX">Sint Maarten (Dutch part)</MenuItem>
                                    <MenuItem value="SK">Slovakia</MenuItem>
                                    <MenuItem value="SI">Slovenia</MenuItem>
                                    <MenuItem value="SB">Solomon Islands</MenuItem>
                                    <MenuItem value="SO">Somalia</MenuItem>
                                    <MenuItem value="ZA">South Africa</MenuItem>
                                    <MenuItem value="GS">South Georgia and the South Sandwich Islands</MenuItem>
                                    <MenuItem value="SS">South Sudan</MenuItem>
                                    <MenuItem value="ES">Spain</MenuItem>
                                    <MenuItem value="LK">Sri Lanka</MenuItem>
                                    <MenuItem value="SD">Sudan</MenuItem>
                                    <MenuItem value="SR">Suriname</MenuItem>
                                    <MenuItem value="SJ">Svalbard and Jan Mayen</MenuItem>
                                    <MenuItem value="SZ">Swaziland</MenuItem>
                                    <MenuItem value="SE">Sweden</MenuItem>
                                    <MenuItem value="CH">Switzerland</MenuItem>
                                    <MenuItem value="SY">Syrian Arab Republic</MenuItem>
                                    <MenuItem value="TW">Taiwan, Province of China</MenuItem>
                                    <MenuItem value="TJ">Tajikistan</MenuItem>
                                    <MenuItem value="TZ">Tanzania, United Republic of</MenuItem>
                                    <MenuItem value="TH">Thailand</MenuItem>
                                    <MenuItem value="TL">Timor-Leste</MenuItem>
                                    <MenuItem value="TG">Togo</MenuItem>
                                    <MenuItem value="TK">Tokelau</MenuItem>
                                    <MenuItem value="TO">Tonga</MenuItem>
                                    <MenuItem value="TT">Trinidad and Tobago</MenuItem>
                                    <MenuItem value="TN">Tunisia</MenuItem>
                                    <MenuItem value="TR">Turkey</MenuItem>
                                    <MenuItem value="TM">Turkmenistan</MenuItem>
                                    <MenuItem value="TC">Turks and Caicos Islands</MenuItem>
                                    <MenuItem value="TV">Tuvalu</MenuItem>
                                    <MenuItem value="UG">Uganda</MenuItem>
                                    <MenuItem value="UA">Ukraine</MenuItem>
                                    <MenuItem value="AE">United Arab Emirates</MenuItem>
                                    <MenuItem value="GB">United Kingdom</MenuItem>
                                    <MenuItem value="US">United States</MenuItem>
                                    <MenuItem value="UM">United States Minor Outlying Islands</MenuItem>
                                    <MenuItem value="UY">Uruguay</MenuItem>
                                    <MenuItem value="UZ">Uzbekistan</MenuItem>
                                    <MenuItem value="VU">Vanuatu</MenuItem>
                                    <MenuItem value="VE">Venezuela, Bolivarian Republic of</MenuItem>
                                    <MenuItem value="VN">Viet Nam</MenuItem>
                                    <MenuItem value="VG">Virgin Islands, British</MenuItem>
                                    <MenuItem value="VI">Virgin Islands, U.S.</MenuItem>
                                    <MenuItem value="WF">Wallis and Futuna</MenuItem>
                                    <MenuItem value="EH">Western Sahara</MenuItem>
                                    <MenuItem value="YE">Yemen</MenuItem>
                                    <MenuItem value="ZM">Zambia</MenuItem>
                                    <MenuItem value="ZW">Zimbabwe</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Golongan Darah</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    required
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.golDarah}
                                    onChange={handleDropdownInputChange("golDarah")}
                                >
                                    <MenuItem selected disabled>golongan darah</MenuItem>
                                    <MenuItem value="A">A</MenuItem>
                                    <MenuItem value="B">B</MenuItem>
                                    <MenuItem value="AB">AB</MenuItem>
                                    <MenuItem value="O">O</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item md>
                        <Grid item container direction="row" justify="flex-start" spacing={2}>
                            <Grid item><b>Alamat</b></Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Jalan, No, RT/RW</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="contoh: Jl. xxx No. xx, RT xx/xx"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.alamatJalan}
                                    onChange={handleTextInputChange("alamatJalan")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Provinsi</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    required
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.provinsi}
                                    onChange={handleDropdownInputChange("provinsi")}
                                >
                                    <MenuItem value="" selected disabled>provinsi</MenuItem>
                                    {(listProvinsi.semuaprovinsi === null || listProvinsi.semuaprovinsi.length === 0) ? null :
                                        listProvinsi.semuaprovinsi.map(row => (
                                            <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kota/Kabupaten</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    required
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.kotaKabupaten}
                                    onChange={handleDropdownInputChange("kotaKabupaten")}
                                >
                                    <MenuItem value="" selected disabled>kota/kabupaten</MenuItem>
                                    {(listKabupaten.kabupatens === null || listKabupaten.kabupatens.length === 0) ? null :
                                        listKabupaten.kabupatens.map(row => (
                                            <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kecamatan</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    required
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.kecamatan}
                                    onChange={handleDropdownInputChange("kecamatan")}
                                >
                                    <MenuItem value="" selected disabled>kecamatan</MenuItem>
                                    {(listKecamatan.kecamatans === null || listKecamatan.kecamatans.length === 0) ? null :
                                        listKecamatan.kecamatans.map(row => (
                                            <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kelurahan/Desa</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    required
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.kelurahan}
                                    onChange={handleDropdownInputChange("kelurahan")}
                                >
                                    <MenuItem value="" selected disabled>kelurahan/desa</MenuItem>
                                    {(listDesa.desas === null || listDesa.desas.length === 0) ? null :
                                        listDesa.desas.map(row => (
                                            <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kode Pos</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="kode pos"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.kodePos}
                                    onChange={handleTextInputChange("kodePos")}
                                ></TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item container direction="row" justify="flex-start" alignItems="flex-start" spacing={1} className={clsx(classes.buttonControlContainer)}>
                    {(!isSaving && !isFailSaving) ?
                        <React.Fragment>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-green text-white btn-sm letter-spacing ")}
                                    onClick={actionSimpan}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >simpan</Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-yellow text-white btn-sm letter-spacing")}
                                    onClick={actionReset}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >reset</Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-red text-white btn-sm letter-spacing")}
                                    onClick={props.onCancel}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >batal</Button>
                            </Grid>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Snackbar
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                open={isSaving || isFailSaving}
                            >
                                <CustomSnackbarContent
                                    variant={(isSaving) ? "info-process" : (isFailSaving) ? "error" : null}
                                    message={(isSaving) ? "Submitting..." : (isFailSaving) ? failSavingMessage : null}
                                />
                            </Snackbar>
                        </React.Fragment>
                    }
                </Grid>
            </Grid>
        </div >
    );
}

function TambahData(props) {
    const classes = useStyles();

    const calculatedPossibleBirthDate = new Date();
    calculatedPossibleBirthDate.setFullYear(calculatedPossibleBirthDate.getFullYear() - 35, 0, 1);

    let data = {
        "nik": "",
        "namaPengajar": "",
        "tempatLahir": "",
        "tanggalLahir": calculatedPossibleBirthDate,
        "jenisKelamin": "L",
        "noHp": "",
        "alamatJalan": "",
        "kelurahan": "",
        "kecamatan": "",
        "kotaKabupaten": "",
        "provinsi": "",
        "kodePos": "",
        "agama": "islam",
        "golDarah": "A",
        "kewarnegaraan": "ID",
        // "imageType":"",
        // "imageByte" : ""

    }
    const [formState, setFormState] = React.useState(data);

    const [isSaving, setSaving] = React.useState(false);
    const [isFailSaving, setFailSaving] = React.useState(false);
    const [failSavingMessage, setFailSavingMessage] = React.useState("");
    function actionSimpan() {
        var allClear = true;
        setSaving(true);

        Object.entries(formState).forEach((entry) => {
            if ((entry[1] === "" || entry[1] === undefined || entry[1] === null) && entry[0] !== "image") {
                allClear = false;
            }
        });
        if (!allClear) {
            setSaving(false);
            setFailSaving(true);
            let timeout = setTimeout(() => {
                setFailSaving(false);
            }, 5000);
            timeoutPromise = timeout;
            setFailSavingMessage("Input Bertanda * Tidak Boleh Kosong!");

            return;
        }

        var errorStatus = false;
        apiDataPengajarEkskul.postData(formState).then((response) => {

        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                Toast.fire({
                    type: "success",
                    title: "Data Berhasil Ditambah!"
                });
                props.onCancel();
            } else {
                // Toast.fire({
                //     type: "error",
                //     title: "Data Gagal Ditambah!"
                // });
                setFailSaving(true);
                let timeout = setTimeout(() => {
                    setFailSaving(false);
                }, 2000);
                timeoutPromise = timeout;
                setFailSavingMessage("Data Gagal Disimpan!");
            }
        });
    }

    function actionReset() {
        setFormState(data);
    }

    const handleTextInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleNumberChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value.replace(/[^0-9]/g, '') });
    };

    const handleImageInputChange = () => event => {
        var file = event.target.files[0];
        //var filename = ""; filename = file.name;
        var filesize = (file.size / 1024).toFixed(4); // KB

        if (filesize <= 300) {
            setUploadError([false, ""]);

            var reader = new FileReader();
            reader.onloadend = () => {
                setFormState({ ...formState, "image": reader.result });
            };
            reader.readAsDataURL(file);
        } else {
            setUploadError([true, "Ukuran File Terlalu Besar, Maks. 300KB!"]);
            //*
            let timeout = setTimeout(() => {
                setUploadError([false, ""]);
                timeoutPromise = null;
            }, 5000);
            timeoutPromise = timeout;
            //*/
        }
    };
    const handleDropdownInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleRadioInputChange = name => event => {
        setFormState({ ...formState, [name]: event.target.value });
    };
    const handleDateChange = (name) => date => {
        var formattedDate = "";
        var year = "", month = "", day = "";

        year += date.getFullYear();
        month += (date.getMonth() + 1); month = ((month.length < 2) ? "0" : "") + month;
        day += date.getDate(); day = ((day.length < 2) ? "0" : "") + day;

        formattedDate = year + "-" + month + "-" + day;

        setFormState({ ...formState, [name]: formattedDate });
    };
    const [mouseHover, setMouseHover] = React.useState(false);
    const [isUploadError, setUploadError] = React.useState([false, ""]);

    //API alamat
    const [listProvinsi, setListProvinsi] = React.useState({
        "semuaprovinsi": []
    });
    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        apiDaerahIndonesia.getProvinsi().then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setListProvinsi((data) => ({ ...data, "semuaprovinsi": tempData.data.semuaprovinsi }));
            }
        });
    }, [setListProvinsi]);

    const [listKabupaten, setListKabupaten] = React.useState({
        "kabupatens": []
    });
    useEffect(() => {
        if (formState.provinsi !== null && formState.provinsi !== undefined && formState.provinsi !== ""){
            setFormState(i => ({ ...i, kotaKabupaten: "", kecamatan: "", kelurahan: "" }));

            let tempData = {};
            var errorStatus = false;
            apiDaerahIndonesia.getKabupaten(formState.provinsi).then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListKabupaten((data) => ({ ...data, "kabupatens": tempData.data.kabupatens }));
                }
            });
        }
    }, [setListKabupaten, setFormState, formState.provinsi]);

    const [listKecamatan, setListKecamatan] = React.useState({
        "kecamatans": []
    });
    useEffect(() => {
        if (formState.kotaKabupaten !== null && formState.kotaKabupaten !== undefined && formState.kotaKabupaten !== ""){
            setFormState(i => ({ ...i, kecamatan: "", kelurahan: "" }));

            let tempData = {};
            var errorStatus = false;
            apiDaerahIndonesia.getKecamatan(formState.kotaKabupaten).then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListKecamatan((data) => ({ ...data, "kecamatans": tempData.data.kecamatans }));
                }
            });
        }
    }, [setListKecamatan, setFormState, formState.kotaKabupaten]);

    const [listDesa, setListDesa] = React.useState({
        "desas": []
    });
    useEffect(() => {
        if (formState.kecamatan !== null && formState.kecamatan !== undefined && formState.kecamatan !== ""){
            setFormState(i => ({ ...i, kelurahan: "" }));

            let tempData = {};
            var errorStatus = false;
            apiDaerahIndonesia.getDesa(formState.kecamatan).then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
            }).finally(() => {
                if (!errorStatus) {
                    setListDesa((data) => ({ ...data, "desas": tempData.data.desas }));
                }
            });
        }
    }, [setListDesa, setFormState, formState.kecamatan]);

    //* Cleanup
    useEffect(() => {
        return function cleanUp() {
            if (timeoutPromise) {
                setUploadError([false, ""]);
                clearTimeout(timeoutPromise);
                timeoutPromise = null;
            }
        }
    }, []);
    //*/

    return (
        <div className={classes.modalContent}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={isUploadError[0]}
            >
                <CustomSnackbarContent
                    variant="error"
                    message={isUploadError[1]}
                />
            </Snackbar>
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="stretch"
                spacing={0}
            >

                <Grid item container direction="row" justify="center" alignItems="center" spacing={2} style={{ marginBottom: 25 + "px" }}>
                    <Grid item container direction="column" justify="center" alignItems="center" spacing={2}>
                        <Paper style={{ height: 200, width: 150 }}>
                            <Grid container direction="row" justify="center" alignItems="center" style={{ height: 100 + "%", width: 100 + "%" }} onMouseEnter={() => { setMouseHover(true) }} onMouseLeave={() => { setMouseHover(false) }}>
                                <Grid item>
                                    {
                                        (formState.image !== "" && formState.image !== null && !mouseHover) ?
                                            <img style={{ maxHeight: "200px", maxWidth: "150px" }} src={formState.image} alt="" />
                                            :
                                            <Button
                                                className={clsx(classes.buttonControl, "btn-rounded btn-sm letter-spacing")}
                                                disabled={false}
                                                variant="outlined"
                                                component="label"
                                                htmlFor="photoUpload"
                                            >unggah</Button>
                                    }
                                    <input type="file" id="photoUpload" style={{ display: "none" }} onChange={handleImageInputChange("image")} accept=".jpe, .jpg, .jpeg, .png, .bmp" />
                                </Grid>
                            </Grid>
                        </Paper>
                        <Grid item>
                            <Typography style={{ color: "red", fontSize: "12px" }}>Ukuran maks. file 300KB</Typography>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item container direction="row" justify="center" alignItems="flex-start" spacing={2}>
                    <Grid item md>
                        <Grid item container direction="row" justify="flex-start" spacing={2}>
                            <Grid item><b>Biodata</b></Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>NIK</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="nik"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.nik}
                                    onChange={handleNumberChange("nik")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Nama</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="nama pengajar"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.namaPengajar}
                                    onChange={handleTextInputChange("namaPengajar")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Tempat Lahir</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="tempat lahir"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.tempatLahir}
                                    onChange={handleTextInputChange("tempatLahir")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Tanggal Lahir</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <DatePicker
                                        required
                                        disableToolbar
                                        disableFuture
                                        variant="inline"
                                        format="yyyy-MM-dd"
                                        margin="normal"
                                        id="date-picker-inline"
                                        className={clsx(classes.textField, classes.dense)}
                                        openTo="YEAR"
                                        views={["year", "month", "date"]}
                                        value={formState.tanggalLahir}
                                        onChange={handleDateChange("tanggalLahir")}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position={'end'}>
                                                    <IconButton>
                                                        <TodayIcon />
                                                    </IconButton>
                                                </InputAdornment>)
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Jenis Kelamin</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item container direction="column" justify="center" alignItems="flex-start" spacing={2} md>
                                <Grid item>
                                    <RadioGroup aria-label="position" name="position" value={formState.jenisKelamin} onChange={handleRadioInputChange("jenisKelamin")} row>
                                        <FormControlLabel
                                            value="L"
                                            control={<Radio color="primary" icon={<RadioButtonUncheckedIcon fontSize="small" />} checkedIcon={<RadioButtonCheckedIcon fontSize="small" />} />}
                                            label="L"
                                            labelPlacement="end"
                                            style={{ marginBottom: 0 }}
                                        />
                                        <FormControlLabel
                                            value="P"
                                            control={<Radio color="primary" icon={<RadioButtonUncheckedIcon fontSize="small" />} checkedIcon={<RadioButtonCheckedIcon fontSize="small" />} />}
                                            label="P"
                                            labelPlacement="end"
                                            style={{ marginBottom: 0 }}
                                        />
                                    </RadioGroup>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>No. HP</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="nomor handphone"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.noHp}
                                    onChange={handleNumberChange("noHp")}
                                    inputProps={{
                                        maxLength: 13,
                                    }}
                                    InputProps={{
                                        startAdornment: <InputAdornment position="start" style={{ paddingBottom: 5 + "px" }}>+62</InputAdornment>,
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Agama</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    required
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.agama}
                                    onChange={handleDropdownInputChange("agama")}
                                >
                                    <MenuItem selected disabled>agama</MenuItem>
                                    <MenuItem value="islam">Islam</MenuItem>
                                    <MenuItem value="kristen">Kristen</MenuItem>
                                    <MenuItem value="katholik">Katholik</MenuItem>
                                    <MenuItem value="hindu">Hindu</MenuItem>
                                    <MenuItem value="buddha">Buddha</MenuItem>
                                    <MenuItem value="konghuchu">Konghuchu</MenuItem>
                                    <MenuItem value="lain">Lainnya</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kewarganegaraan</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    required
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.kewarnegaraan}
                                    onChange={handleDropdownInputChange("kewarnegaraan")}
                                >
                                    <MenuItem selected disabled>kewarganegaraan</MenuItem>
                                    <MenuItem value="AF">Afghanistan</MenuItem>
                                    <MenuItem value="AX">Åland Islands</MenuItem>
                                    <MenuItem value="AL">Albania</MenuItem>
                                    <MenuItem value="DZ">Algeria</MenuItem>
                                    <MenuItem value="AS">American Samoa</MenuItem>
                                    <MenuItem value="AD">Andorra</MenuItem>
                                    <MenuItem value="AO">Angola</MenuItem>
                                    <MenuItem value="AI">Anguilla</MenuItem>
                                    <MenuItem value="AQ">Antarctica</MenuItem>
                                    <MenuItem value="AG">Antigua and Barbuda</MenuItem>
                                    <MenuItem value="AR">Argentina</MenuItem>
                                    <MenuItem value="AM">Armenia</MenuItem>
                                    <MenuItem value="AW">Aruba</MenuItem>
                                    <MenuItem value="AU">Australia</MenuItem>
                                    <MenuItem value="AT">Austria</MenuItem>
                                    <MenuItem value="AZ">Azerbaijan</MenuItem>
                                    <MenuItem value="BS">Bahamas</MenuItem>
                                    <MenuItem value="BH">Bahrain</MenuItem>
                                    <MenuItem value="BD">Bangladesh</MenuItem>
                                    <MenuItem value="BB">Barbados</MenuItem>
                                    <MenuItem value="BY">Belarus</MenuItem>
                                    <MenuItem value="BE">Belgium</MenuItem>
                                    <MenuItem value="BZ">Belize</MenuItem>
                                    <MenuItem value="BJ">Benin</MenuItem>
                                    <MenuItem value="BM">Bermuda</MenuItem>
                                    <MenuItem value="BT">Bhutan</MenuItem>
                                    <MenuItem value="BO">Bolivia, Plurinational State of</MenuItem>
                                    <MenuItem value="BQ">Bonaire, Sint Eustatius and Saba</MenuItem>
                                    <MenuItem value="BA">Bosnia and Herzegovina</MenuItem>
                                    <MenuItem value="BW">Botswana</MenuItem>
                                    <MenuItem value="BV">Bouvet Island</MenuItem>
                                    <MenuItem value="BR">Brazil</MenuItem>
                                    <MenuItem value="IO">British Indian Ocean Territory</MenuItem>
                                    <MenuItem value="BN">Brunei Darussalam</MenuItem>
                                    <MenuItem value="BG">Bulgaria</MenuItem>
                                    <MenuItem value="BF">Burkina Faso</MenuItem>
                                    <MenuItem value="BI">Burundi</MenuItem>
                                    <MenuItem value="KH">Cambodia</MenuItem>
                                    <MenuItem value="CM">Cameroon</MenuItem>
                                    <MenuItem value="CA">Canada</MenuItem>
                                    <MenuItem value="CV">Cape Verde</MenuItem>
                                    <MenuItem value="KY">Cayman Islands</MenuItem>
                                    <MenuItem value="CF">Central African Republic</MenuItem>
                                    <MenuItem value="TD">Chad</MenuItem>
                                    <MenuItem value="CL">Chile</MenuItem>
                                    <MenuItem value="CN">China</MenuItem>
                                    <MenuItem value="CX">Christmas Island</MenuItem>
                                    <MenuItem value="CC">Cocos (Keeling) Islands</MenuItem>
                                    <MenuItem value="CO">Colombia</MenuItem>
                                    <MenuItem value="KM">Comoros</MenuItem>
                                    <MenuItem value="CG">Congo</MenuItem>
                                    <MenuItem value="CD">Congo, the Democratic Republic of the</MenuItem>
                                    <MenuItem value="CK">Cook Islands</MenuItem>
                                    <MenuItem value="CR">Costa Rica</MenuItem>
                                    <MenuItem value="CI">Côte d'Ivoire</MenuItem>
                                    <MenuItem value="HR">Croatia</MenuItem>
                                    <MenuItem value="CU">Cuba</MenuItem>
                                    <MenuItem value="CW">Curaçao</MenuItem>
                                    <MenuItem value="CY">Cyprus</MenuItem>
                                    <MenuItem value="CZ">Czech Republic</MenuItem>
                                    <MenuItem value="DK">Denmark</MenuItem>
                                    <MenuItem value="DJ">Djibouti</MenuItem>
                                    <MenuItem value="DM">Dominica</MenuItem>
                                    <MenuItem value="DO">Dominican Republic</MenuItem>
                                    <MenuItem value="EC">Ecuador</MenuItem>
                                    <MenuItem value="EG">Egypt</MenuItem>
                                    <MenuItem value="SV">El Salvador</MenuItem>
                                    <MenuItem value="GQ">Equatorial Guinea</MenuItem>
                                    <MenuItem value="ER">Eritrea</MenuItem>
                                    <MenuItem value="EE">Estonia</MenuItem>
                                    <MenuItem value="ET">Ethiopia</MenuItem>
                                    <MenuItem value="FK">Falkland Islands (Malvinas)</MenuItem>
                                    <MenuItem value="FO">Faroe Islands</MenuItem>
                                    <MenuItem value="FJ">Fiji</MenuItem>
                                    <MenuItem value="FI">Finland</MenuItem>
                                    <MenuItem value="FR">France</MenuItem>
                                    <MenuItem value="GF">French Guiana</MenuItem>
                                    <MenuItem value="PF">French Polynesia</MenuItem>
                                    <MenuItem value="TF">French Southern Territories</MenuItem>
                                    <MenuItem value="GA">Gabon</MenuItem>
                                    <MenuItem value="GM">Gambia</MenuItem>
                                    <MenuItem value="GE">Georgia</MenuItem>
                                    <MenuItem value="DE">Germany</MenuItem>
                                    <MenuItem value="GH">Ghana</MenuItem>
                                    <MenuItem value="GI">Gibraltar</MenuItem>
                                    <MenuItem value="GR">Greece</MenuItem>
                                    <MenuItem value="GL">Greenland</MenuItem>
                                    <MenuItem value="GD">Grenada</MenuItem>
                                    <MenuItem value="GP">Guadeloupe</MenuItem>
                                    <MenuItem value="GU">Guam</MenuItem>
                                    <MenuItem value="GT">Guatemala</MenuItem>
                                    <MenuItem value="GG">Guernsey</MenuItem>
                                    <MenuItem value="GN">Guinea</MenuItem>
                                    <MenuItem value="GW">Guinea-Bissau</MenuItem>
                                    <MenuItem value="GY">Guyana</MenuItem>
                                    <MenuItem value="HT">Haiti</MenuItem>
                                    <MenuItem value="HM">Heard Island and McDonald Islands</MenuItem>
                                    <MenuItem value="VA">Holy See (Vatican City State)</MenuItem>
                                    <MenuItem value="HN">Honduras</MenuItem>
                                    <MenuItem value="HK">Hong Kong</MenuItem>
                                    <MenuItem value="HU">Hungary</MenuItem>
                                    <MenuItem value="IS">Iceland</MenuItem>
                                    <MenuItem value="IN">India</MenuItem>
                                    <MenuItem value="ID">Indonesia</MenuItem>
                                    <MenuItem value="IR">Iran, Islamic Republic of</MenuItem>
                                    <MenuItem value="IQ">Iraq</MenuItem>
                                    <MenuItem value="IE">Ireland</MenuItem>
                                    <MenuItem value="IM">Isle of Man</MenuItem>
                                    <MenuItem value="IL">Israel</MenuItem>
                                    <MenuItem value="IT">Italy</MenuItem>
                                    <MenuItem value="JM">Jamaica</MenuItem>
                                    <MenuItem value="JP">Japan</MenuItem>
                                    <MenuItem value="JE">Jersey</MenuItem>
                                    <MenuItem value="JO">Jordan</MenuItem>
                                    <MenuItem value="KZ">Kazakhstan</MenuItem>
                                    <MenuItem value="KE">Kenya</MenuItem>
                                    <MenuItem value="KI">Kiribati</MenuItem>
                                    <MenuItem value="KP">Korea, Democratic People's Republic of</MenuItem>
                                    <MenuItem value="KR">Korea, Republic of</MenuItem>
                                    <MenuItem value="KW">Kuwait</MenuItem>
                                    <MenuItem value="KG">Kyrgyzstan</MenuItem>
                                    <MenuItem value="LA">Lao People's Democratic Republic</MenuItem>
                                    <MenuItem value="LV">Latvia</MenuItem>
                                    <MenuItem value="LB">Lebanon</MenuItem>
                                    <MenuItem value="LS">Lesotho</MenuItem>
                                    <MenuItem value="LR">Liberia</MenuItem>
                                    <MenuItem value="LY">Libya</MenuItem>
                                    <MenuItem value="LI">Liechtenstein</MenuItem>
                                    <MenuItem value="LT">Lithuania</MenuItem>
                                    <MenuItem value="LU">Luxembourg</MenuItem>
                                    <MenuItem value="MO">Macao</MenuItem>
                                    <MenuItem value="MK">Macedonia, the former Yugoslav Republic of</MenuItem>
                                    <MenuItem value="MG">Madagascar</MenuItem>
                                    <MenuItem value="MW">Malawi</MenuItem>
                                    <MenuItem value="MY">Malaysia</MenuItem>
                                    <MenuItem value="MV">Maldives</MenuItem>
                                    <MenuItem value="ML">Mali</MenuItem>
                                    <MenuItem value="MT">Malta</MenuItem>
                                    <MenuItem value="MH">Marshall Islands</MenuItem>
                                    <MenuItem value="MQ">Martinique</MenuItem>
                                    <MenuItem value="MR">Mauritania</MenuItem>
                                    <MenuItem value="MU">Mauritius</MenuItem>
                                    <MenuItem value="YT">Mayotte</MenuItem>
                                    <MenuItem value="MX">Mexico</MenuItem>
                                    <MenuItem value="FM">Micronesia, Federated States of</MenuItem>
                                    <MenuItem value="MD">Moldova, Republic of</MenuItem>
                                    <MenuItem value="MC">Monaco</MenuItem>
                                    <MenuItem value="MN">Mongolia</MenuItem>
                                    <MenuItem value="ME">Montenegro</MenuItem>
                                    <MenuItem value="MS">Montserrat</MenuItem>
                                    <MenuItem value="MA">Morocco</MenuItem>
                                    <MenuItem value="MZ">Mozambique</MenuItem>
                                    <MenuItem value="MM">Myanmar</MenuItem>
                                    <MenuItem value="NA">Namibia</MenuItem>
                                    <MenuItem value="NR">Nauru</MenuItem>
                                    <MenuItem value="NP">Nepal</MenuItem>
                                    <MenuItem value="NL">Netherlands</MenuItem>
                                    <MenuItem value="NC">New Caledonia</MenuItem>
                                    <MenuItem value="NZ">New Zealand</MenuItem>
                                    <MenuItem value="NI">Nicaragua</MenuItem>
                                    <MenuItem value="NE">Niger</MenuItem>
                                    <MenuItem value="NG">Nigeria</MenuItem>
                                    <MenuItem value="NU">Niue</MenuItem>
                                    <MenuItem value="NF">Norfolk Island</MenuItem>
                                    <MenuItem value="MP">Northern Mariana Islands</MenuItem>
                                    <MenuItem value="NO">Norway</MenuItem>
                                    <MenuItem value="OM">Oman</MenuItem>
                                    <MenuItem value="PK">Pakistan</MenuItem>
                                    <MenuItem value="PW">Palau</MenuItem>
                                    <MenuItem value="PS">Palestinian Territory, Occupied</MenuItem>
                                    <MenuItem value="PA">Panama</MenuItem>
                                    <MenuItem value="PG">Papua New Guinea</MenuItem>
                                    <MenuItem value="PY">Paraguay</MenuItem>
                                    <MenuItem value="PE">Peru</MenuItem>
                                    <MenuItem value="PH">Philippines</MenuItem>
                                    <MenuItem value="PN">Pitcairn</MenuItem>
                                    <MenuItem value="PL">Poland</MenuItem>
                                    <MenuItem value="PT">Portugal</MenuItem>
                                    <MenuItem value="PR">Puerto Rico</MenuItem>
                                    <MenuItem value="QA">Qatar</MenuItem>
                                    <MenuItem value="RE">Réunion</MenuItem>
                                    <MenuItem value="RO">Romania</MenuItem>
                                    <MenuItem value="RU">Russian Federation</MenuItem>
                                    <MenuItem value="RW">Rwanda</MenuItem>
                                    <MenuItem value="BL">Saint Barthélemy</MenuItem>
                                    <MenuItem value="SH">Saint Helena, Ascension and Tristan da Cunha</MenuItem>
                                    <MenuItem value="KN">Saint Kitts and Nevis</MenuItem>
                                    <MenuItem value="LC">Saint Lucia</MenuItem>
                                    <MenuItem value="MF">Saint Martin (French part)</MenuItem>
                                    <MenuItem value="PM">Saint Pierre and Miquelon</MenuItem>
                                    <MenuItem value="VC">Saint Vincent and the Grenadines</MenuItem>
                                    <MenuItem value="WS">Samoa</MenuItem>
                                    <MenuItem value="SM">San Marino</MenuItem>
                                    <MenuItem value="ST">Sao Tome and Principe</MenuItem>
                                    <MenuItem value="SA">Saudi Arabia</MenuItem>
                                    <MenuItem value="SN">Senegal</MenuItem>
                                    <MenuItem value="RS">Serbia</MenuItem>
                                    <MenuItem value="SC">Seychelles</MenuItem>
                                    <MenuItem value="SL">Sierra Leone</MenuItem>
                                    <MenuItem value="SG">Singapore</MenuItem>
                                    <MenuItem value="SX">Sint Maarten (Dutch part)</MenuItem>
                                    <MenuItem value="SK">Slovakia</MenuItem>
                                    <MenuItem value="SI">Slovenia</MenuItem>
                                    <MenuItem value="SB">Solomon Islands</MenuItem>
                                    <MenuItem value="SO">Somalia</MenuItem>
                                    <MenuItem value="ZA">South Africa</MenuItem>
                                    <MenuItem value="GS">South Georgia and the South Sandwich Islands</MenuItem>
                                    <MenuItem value="SS">South Sudan</MenuItem>
                                    <MenuItem value="ES">Spain</MenuItem>
                                    <MenuItem value="LK">Sri Lanka</MenuItem>
                                    <MenuItem value="SD">Sudan</MenuItem>
                                    <MenuItem value="SR">Suriname</MenuItem>
                                    <MenuItem value="SJ">Svalbard and Jan Mayen</MenuItem>
                                    <MenuItem value="SZ">Swaziland</MenuItem>
                                    <MenuItem value="SE">Sweden</MenuItem>
                                    <MenuItem value="CH">Switzerland</MenuItem>
                                    <MenuItem value="SY">Syrian Arab Republic</MenuItem>
                                    <MenuItem value="TW">Taiwan, Province of China</MenuItem>
                                    <MenuItem value="TJ">Tajikistan</MenuItem>
                                    <MenuItem value="TZ">Tanzania, United Republic of</MenuItem>
                                    <MenuItem value="TH">Thailand</MenuItem>
                                    <MenuItem value="TL">Timor-Leste</MenuItem>
                                    <MenuItem value="TG">Togo</MenuItem>
                                    <MenuItem value="TK">Tokelau</MenuItem>
                                    <MenuItem value="TO">Tonga</MenuItem>
                                    <MenuItem value="TT">Trinidad and Tobago</MenuItem>
                                    <MenuItem value="TN">Tunisia</MenuItem>
                                    <MenuItem value="TR">Turkey</MenuItem>
                                    <MenuItem value="TM">Turkmenistan</MenuItem>
                                    <MenuItem value="TC">Turks and Caicos Islands</MenuItem>
                                    <MenuItem value="TV">Tuvalu</MenuItem>
                                    <MenuItem value="UG">Uganda</MenuItem>
                                    <MenuItem value="UA">Ukraine</MenuItem>
                                    <MenuItem value="AE">United Arab Emirates</MenuItem>
                                    <MenuItem value="GB">United Kingdom</MenuItem>
                                    <MenuItem value="US">United States</MenuItem>
                                    <MenuItem value="UM">United States Minor Outlying Islands</MenuItem>
                                    <MenuItem value="UY">Uruguay</MenuItem>
                                    <MenuItem value="UZ">Uzbekistan</MenuItem>
                                    <MenuItem value="VU">Vanuatu</MenuItem>
                                    <MenuItem value="VE">Venezuela, Bolivarian Republic of</MenuItem>
                                    <MenuItem value="VN">Viet Nam</MenuItem>
                                    <MenuItem value="VG">Virgin Islands, British</MenuItem>
                                    <MenuItem value="VI">Virgin Islands, U.S.</MenuItem>
                                    <MenuItem value="WF">Wallis and Futuna</MenuItem>
                                    <MenuItem value="EH">Western Sahara</MenuItem>
                                    <MenuItem value="YE">Yemen</MenuItem>
                                    <MenuItem value="ZM">Zambia</MenuItem>
                                    <MenuItem value="ZW">Zimbabwe</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Golongan Darah</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <Select
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    required
                                    style={{ margin: 0 }}
                                    displayEmpty
                                    value={formState.golDarah}
                                    onChange={handleDropdownInputChange("golDarah")}
                                >
                                    <MenuItem selected disabled>golongan darah</MenuItem>
                                    <MenuItem value="A">A</MenuItem>
                                    <MenuItem value="B">B</MenuItem>
                                    <MenuItem value="AB">AB</MenuItem>
                                    <MenuItem value="O">O</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item md>
                        <Grid item container direction="row" justify="flex-start" spacing={2}>
                            <Grid item><b>Alamat</b></Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Jalan, No, RT/RW</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="contoh: Jl. xxx No. xx, RT xx/xx"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.alamatJalan}
                                    onChange={handleTextInputChange("alamatJalan")}
                                ></TextField>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Provinsi</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                            <Select
                                        className={clsx(classes.textField, classes.dense)}
                                        margin="dense"
                                        required
                                        style={{ margin: 0 }}
                                        displayEmpty
                                        value={formState.provinsi}
                                        onChange={handleDropdownInputChange("provinsi")}
                                    >
                                        <MenuItem value="" selected disabled>provinsi</MenuItem>
                                        {(listProvinsi.semuaprovinsi === null || listProvinsi.semuaprovinsi.length === 0) ? null :
                                            listProvinsi.semuaprovinsi.map(row => (
                                                <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                            ))
                                        }
                                    </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kota/Kabupaten</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                            <Select
                                        className={clsx(classes.textField, classes.dense)}
                                        margin="dense"
                                        required
                                        style={{ margin: 0 }}
                                        displayEmpty
                                        value={formState.kotaKabupaten}
                                        onChange={handleDropdownInputChange("kotaKabupaten")}
                                    >
                                        <MenuItem value="" selected disabled>kota/kabupaten</MenuItem>
                                        {(listKabupaten.kabupatens === null || listKabupaten.kabupatens.length === 0) ? null :
                                            listKabupaten.kabupatens.map(row => (
                                                <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                            ))
                                        }
                                    </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kecamatan</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                            <Select
                                        className={clsx(classes.textField, classes.dense)}
                                        margin="dense"
                                        required
                                        style={{ margin: 0 }}
                                        displayEmpty
                                        value={formState.kecamatan}
                                        onChange={handleDropdownInputChange("kecamatan")}
                                    >
                                        <MenuItem value="" selected disabled>kecamatan</MenuItem>
                                        {(listKecamatan.kecamatans === null || listKecamatan.kecamatans.length === 0) ? null :
                                            listKecamatan.kecamatans.map(row => (
                                                <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                            ))
                                        }
                                    </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kelurahan/Desa</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                            <Select
                                        className={clsx(classes.textField, classes.dense)}
                                        margin="dense"
                                        required
                                        style={{ margin: 0 }}
                                        displayEmpty
                                        value={formState.kelurahan}
                                        onChange={handleDropdownInputChange("kelurahan")}
                                    >
                                        <MenuItem value="" selected disabled>kelurahan/desa</MenuItem>
                                        {(listDesa.desas === null || listDesa.desas.length === 0) ? null :
                                            listDesa.desas.map(row => (
                                                <MenuItem key={row.id} value={row.id}>{row.nama}</MenuItem>
                                            ))
                                        }
                                    </Select>
                            </Grid>
                        </Grid>
                        <Grid item container direction="row" justify="flex-start" alignItems="stretch" spacing={2}>
                            <Grid item md={5} className="vertical-center-text"><span style={{ color: "red" }}>* </span>Kode Pos</Grid>
                            <Grid item className="vertical-center-text">:</Grid>
                            <Grid item md>
                                <TextField
                                    required
                                    id="outlined-dense"
                                    placeholder="kode pos"
                                    className={clsx(classes.textField, classes.dense)}
                                    margin="dense"
                                    style={{ margin: 0 }}
                                    value={formState.kodePos}
                                    onChange={handleTextInputChange("kodePos")}
                                ></TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item container direction="row" justify="flex-start" alignItems="flex-start" spacing={1} className={clsx(classes.buttonControlContainer)}>
                    {(!isSaving && !isFailSaving) ?
                        <React.Fragment>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-green text-white btn-sm letter-spacing ")}
                                    onClick={actionSimpan}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >simpan</Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-yellow text-white btn-sm letter-spacing")}
                                    onClick={actionReset}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >reset</Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={clsx(classes.buttonControl, "btn-rounded bg-red text-white btn-sm letter-spacing")}
                                    onClick={props.onCancel}
                                    disabled={false}
                                    component={Paper}
                                    elevation={2}
                                >batal</Button>
                            </Grid>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Snackbar
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                open={isSaving || isFailSaving}
                            >
                                <CustomSnackbarContent
                                    variant={(isSaving) ? "info-process" : (isFailSaving) ? "error" : null}
                                    message={(isSaving) ? "Submitting..." : (isFailSaving) ? failSavingMessage : null}
                                />
                            </Snackbar>
                        </React.Fragment>
                    }
                </Grid>
            </Grid>
        </div >
    );
}


const CustomCircularProgress = withStyles({
    root: {
        color: '#517296',
    },
})(CircularProgress);
const CustomLinearProgress = withStyles({
    colorPrimary: {
        height: 4 + "px",
        borderRadius: "0px 0px 4px 4px",
        backgroundColor: '#d3d3d3',
    },
    barColorPrimary: {
        borderRadius: 25 + "px",
        backgroundColor: '#517296',
    },
})(LinearProgress);

// SnackBar
const LoadingIcon = withStyles(theme => ({
    root: {
        color: amber[100],
    },
}))(CircularProgress);
/*
LoadingIcon.defaultProps={
    size: 25,
    thickness: 4,
}
//*/
const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
    "info-process": WarningIcon,
};
const useSnackBarStyles = makeStyles(theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    "info-process": {
        backgroundColor: amber[800],
        color: theme.palette.common.white,
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));
function CustomSnackbarContent(props) {
    const classes = useSnackBarStyles();
    const { className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={clsx(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.iconVariant)} />
                    {message}
                    {(variant === "info-process") ? <LoadingIcon style={{ position: "absolute", right: 20 + "px" }} size={25} thickness={4} /> : null}
                </span>
            }
            {...other}
        />
    );
}
CustomSnackbarContent.propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['error', 'info', 'success', 'warning', 'info-process']).isRequired,
}
// SnackBar

// Components

function DataStatus() {
    const classes = useStyles();

    const [filterBy, setFilterBy] = React.useState("nik");
    function handleChangeFilter(event) {
        setFilterBy(event.target.value);
    }

    const [filterValue, setFilterValue] = React.useState("");
    function handleChangeFilterValue(event) {
        setFilterValue(event.target.value);
    }

    const [sortBy, setSortBy] = React.useState("nik");
    function handleChangeSortBy(event) {
        setSortBy(event.target.value);
    }

    const [isSortAscending, setSortAscending] = React.useState(true);
    function handleChangeSortAscending() {
        setSortAscending(!isSortAscending);
    }

    const [page, setPage] = React.useState(0);
    function handleChangePage(event, newPage) {
        setPage(newPage);
    }

    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    function handleChangeRowsPerPage(event) {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
        delete: false,
    });
    const toggleDrawer = (side, open) => event => {
        setState({ ...state, [side]: open });
        setselectedId(selectedId || "");
    };
    const toggleDrawerModalContent = (side, open, modalContent, selectedId) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setState({ ...state, [side]: open });
        setModalContent(modalContent);
        setselectedId(selectedId || "");
    };

    const [modalContent, setModalContent] = React.useState("");
    const [selectedId, setselectedId] = React.useState("");
    const [filterOl, setFilterOl] = React.useState({
        "data": []
    });
    const { tableDatas, setTableDatas } = useTableData({ "totalItems": 0, "items": [] });
    const [isFetching, setFetching] = React.useState(true);

    useEffect(() => {
        if (!state.top && !state.left && !state.bottom && !state.right && !state.delete) {
            setFetching(true);

            let tempData = {};
            let errorObj = {};
            var errorStatus = false;

            apiDataPengajarEkskul.cancel && apiDataPengajarEkskul.cancel();
            var requestPromise = apiDataPengajarEkskul.getData(((filterValue !== "") ? `LOWER(CAST(${filterBy} AS text)) LIKE LOWER('%25${filterValue}%25')` : ""), `${sortBy} ${(isSortAscending) ? "ASC" : "DESC"}`, page, rowsPerPage);

            requestPromise.then((response) => {
                tempData = response;
            }).catch((error) => {
                errorStatus = true;
                errorObj = error;
            }).finally(() => {
                if (!errorStatus) {
                    setTableDatas((data) => ({ ...data, "totalItems": tempData.data.totalItems, "items": tempData.data.items }));
                }
                if (!errorObj.__CANCEL__) {
                    setFetching(false);
                }
            });
        }
    }, [filterValue, filterBy, isSortAscending, sortBy, page, rowsPerPage, setTableDatas, state, setFetching]);

    useEffect(() => {
        let tempData = {};
        var errorStatus = false;
        optionLists.getOlDataPengajarEkskul("", "", "", "").then((response) => {
            tempData = response;
        }).catch((error) => {
            errorStatus = true;
        }).finally(() => {
            if (!errorStatus) {
                setFilterOl((data) => ({ ...data, "data": tempData.data }));
            }
        });
    }, [setFilterOl]);

    const [isDeleting, setDeleting] = React.useState(false);
    function actionHapus() {
        setDeleting(true);
        apiDataPengajarEkskul.deleteData(selectedId).then((response) => {
            Toast.fire({
                type: "success",
                title: "Berhasil Dihapus!"
            })
        }).catch((error) => {
            Toast.fire({
                type: "error",
                title: "Proses Menghapus Gagal!"
            });
        }).finally(() => {
            setDeleting(false);
            setState({ ...state, "delete": false });
        });
    }

    //Cleanup function
    useEffect(() => {
        return function cleanUp() {
            if (apiDataPengajarEkskul.cancel && apiDataPengajarEkskul.cancel()) {
                apiDataPengajarEkskul.cancel = null;
            }

            if (optionLists.cancel && optionLists.cancel()) {
                console.log("AAA");
                optionLists.cancel = null;
            }
        }
    }, []);

    return (
        <div>
            <Box className={classes.toolbar}>

                <Box flexGrow={1}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <Select
                            value={filterBy}
                            onChange={handleChangeFilter}
                            name="filterBy"
                            displayEmpty
                            className={classes.filterSelect}>
                            {(filterOl.data === null || filterOl.data.length === 0) ?
                                <MenuItem>filter</MenuItem> :
                                filterOl.data.map(row => (
                                    <MenuItem key={row.key} value={row.values.key}>{row.values.value}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <FormControl className={clsx(classes.formControl)}>
                        <Input
                            value={filterValue}
                            onChange={handleChangeFilterValue}
                            className={classes.filterInput}
                            placeholder="Filter"
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton>
                                        <SearchIcon className={classes.searchButton} />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Box>

                <FormControl variant="outlined" className={classes.formControl}>
                    <Tooltip title={isSortAscending ? "Ascending" : "Descending"}>
                        <IconButton onClick={handleChangeSortAscending}>
                            <SortIcon className={isSortAscending ? "" : classes.rotateHalf} />
                        </IconButton>
                    </Tooltip>
                </FormControl>

                <FormControl variant="outlined" className={classes.formControl}>
                    <Select
                        value={sortBy}
                        onChange={handleChangeSortBy}
                        name="sortBy"
                        displayEmpty
                        className={classes.filterSelect}>

                        {(filterOl.data === null || filterOl.data.length === 0) ? <MenuItem>filter</MenuItem> :
                            filterOl.data.map(row => (
                                <MenuItem key={row.key} value={row.values.key}>{row.values.value}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>

                <FormControl variant="outlined" className={classes.formControl}>
                    <Button onClick={toggleDrawerModalContent('right', true, "tambah")}>
                        <AddIcon style={{ marginRight: 5 + "pt" }} />
                        Tambah Data
                    </Button>
                </FormControl>

            </Box>

            <Paper className={classes.tableRoot}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow className={classes.tableHeader}>

                            <TableCell style={{ width: 40 + "px" }}></TableCell>
                            <TableCell>ID Status</TableCell>
                            <TableCell align="left">Nama Status Pekerjaan</TableCell>
                            <TableCell align="center">Aksi</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(tableDatas.items === null || tableDatas.items.length === 0) ? <TableRow><TableCell align="center" colSpan={6}>{(isFetching) ? "Sedang Memuat ..." : "Tidak Ada Data"}</TableCell></TableRow> :
                            tableDatas.items.map(row => (
                                <TableRow key={row.rowNumber}>
                                    <TableCell scope="row">
                                        <Avatar style={{ height: 40 + "px", width: 40 + "px" }} src={row.image} />
                                    </TableCell>
                                    <TableCell component="th">
                                        <Link onClick={toggleDrawerModalContent("right", true, "detail", row.idPengajarEskul)} style={{ color: "#48d" }}>{row.nik}</Link>
                                    </TableCell>
                                    <TableCell align="left">{row.namaPengajar}</TableCell>
                                    <TableCell align="center">{row.ekstrakulikuler}</TableCell>
                                    <TableCell align="center">
                                        <div className={classes.buttonContainer}>
                                            <Button
                                                variant="outlined" size="small"
                                                className={clsx(classes.buttonEdit, "bg-yellow")}
                                                onClick={toggleDrawerModalContent('right', true, "edit", row.idPengajarEskul)}
                                                disabled={false}
                                            >ubah</Button>
                                            <Button
                                                variant="outlined" size="small"
                                                className={clsx(classes.buttonDelete, "bg-red")}
                                                onClick={toggleDrawerModalContent('delete', true, "", row.idPengajarEskul)}
                                                disabled={false}
                                            >hapus</Button>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
                {(isFetching) ? <CustomLinearProgress /> : null}
            </Paper>

            <Box className={classes.toolbarFoot}>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25, 50, 100]}
                    component="div"
                    count={tableDatas.totalItems}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'previous page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'next page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Box>

            <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false)}>
                <Container maxWidth={"lg"} className={classes.drawerModal}>
                    <Box className={classes.toolbar} style={{ backgroundColor: "#002F48", color: "#fff" }}>
                        <Typography className={classes.pageTitle}>
                            {
                                (modalContent === "edit") ? "Ubah Data Status" :
                                    (modalContent === "tambah") ? "Tambah Data Status" :
                                        "Detail Data Status"
                            }
                        </Typography>
                    </Box>
                    {
                        (modalContent === "edit") ? <EditData id={selectedId} onCancel={toggleDrawer("right", false)} /> :
                            (modalContent === "tambah") ? <TambahData onCancel={toggleDrawer("right", false)} /> :
                                <DetailData id={selectedId} />
                    }
                </Container>
            </Drawer>

            <Dialog
                open={state.delete}
                onClose={toggleDrawer('delete', false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle style={{ width: 250 + "pt" }} id="alert-dialog-title">{"Hapus Data?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    {(isDeleting) ? <CustomCircularProgress size={30} thickness={4} /> :
                        <React.Fragment>
                            <Button
                                onClick={toggleDrawer('delete', false)}
                            >tidak</Button>
                            <Button
                                className={clsx(classes.buttonDelete, "bg-red")}
                                onClick={() => { actionHapus(selectedId.idPublikOrtu) }}
                            >hapus</Button>
                        </React.Fragment>
                    }
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default DataStatus;
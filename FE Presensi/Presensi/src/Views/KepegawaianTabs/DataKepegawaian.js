import React, { useState } from "react";

import { Container, Card, CardContent, Link } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

import { DataKaryawan } from "./KaryawanContent";
import { DataStatus } from "./StatusContent";

//var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

const useStyles = makeStyles(theme => ({
    linkNavigationContainer: {
        padding: 0,
        width: "100%",
        display: "flex",
        alignItems: "left",
        justifyItems: "flex-start",
    },
    linkNavigation: {
        margin: 10 + "pt",
        fontSize: 12 + "pt",
    },
    linkNavigationActive: {
        borderBottom: "2pt solid #0182C6"
    },
    contentCard: {

    }
}));

function DataKepegawaian() {
    const classes = useStyles();
    const [activeContent, setActiveContent] = useState("DataKaryawan");

    function handleClick(linkNav) {
        setActiveContent(linkNav);
    }

    return (
        <div>
            <Container maxWidth={false} className={classes.linkNavigationContainer}>
                <Link className={classes.linkNavigation + ((activeContent === "DataKaryawan") ? " " + classes.linkNavigationActive + " mb-2" : "")} onClick={() => (handleClick("DataKaryawan"))}>Data Karyawan</Link>
                <Link className={classes.linkNavigation + ((activeContent === "DataStatus") ? " " + classes.linkNavigationActive + " mb-2" : "")} onClick={() => (handleClick("DataStatus"))}>Data Status</Link>
            </Container>
            <Card raised={false} className={classes.contentCard}>
                <CardContent>
                    {
                        (activeContent === "DataKaryawan") ? <DataKaryawan /> : 
                        (activeContent === "DataStatus") ? <DataStatus /> : null
                    }
                </CardContent>
            </Card>
        </div>
    );
}

export default DataKepegawaian;
import React from "react";

const useDrawer = () => {
    const [drawerState, setDrawerState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
        delete: false,
    });

    const toggleDrawer = (side, open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setDrawerState({ ...drawerState, [side]: open });
    };
    const toggleDrawerModalContent = (side, open, modalContent) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setDrawerState({ ...drawerState, [side]: open });
        setModalContent(modalContent);
    };

    const [modalContent, setModalContent] = React.useState("");

    return {
        drawerState,
        toggleDrawer, toggleDrawerModalContent,
        modalContent
    };
};

export default useDrawer;
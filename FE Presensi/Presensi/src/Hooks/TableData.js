import React from "react";

const useTableData=(object)=>{
    const [tableDatas, setTableDatas] = React.useState(object || {});
    
    return { tableDatas, setTableDatas };
};

export default useTableData;
import React from "react";
import { Table, TableHead, TableRow, TableCell, TableBody, Button } from "@material-ui/core";

function DataTable(){
    return(
        <Table className={classes.table}>
            <TableHead>
                <TableRow>

                    <TableCell style={{ width: 40 + "px" }}></TableCell>
                    <TableCell>Nomor Induk Siswa</TableCell>
                    <TableCell align="left">Nama Siswa</TableCell>
                    <TableCell align="right">Rombel</TableCell>
                    <TableCell align="right">Angkatan</TableCell>
                    <TableCell align="center">Aksi</TableCell>

                </TableRow>
            </TableHead>
            <TableBody>

                {rows.map(row => (
                    <TableRow key={row.name}>
                        <TableCell scope="row">
                            <div style={{ width: "40px", height: "40px", borderRadius: "50%", background: "black" }} />
                        </TableCell>
                        <TableCell component="th">
                            <Link onClick={toggleDrawerModalContent('right', true, "detail")}>{row.nis}</Link>
                        </TableCell>
                        <TableCell align="left">{row.name}</TableCell>
                        <TableCell align="right">{row.rombel}</TableCell>
                        <TableCell align="right">{row.angkatan}</TableCell>

                        <TableCell align="center">
                            <div className={classes.buttonContainer}>
                                <Button
                                    variant="outlined" size="small"
                                    className={classes.buttonEdit}
                                    onClick={toggleDrawerModalContent('right', true, "edit")}
                                    disabled={false}
                                >edit</Button>
                                <Button
                                    variant="outlined" size="small"
                                    className={classes.buttonDelete}
                                    onClick={toggleDrawer('delete', true)}
                                    disabled={false}
                                >delete</Button>
                            </div>
                        </TableCell>
                    </TableRow>
                ))}

            </TableBody>
        </Table>
    );
}

export default DataTable;
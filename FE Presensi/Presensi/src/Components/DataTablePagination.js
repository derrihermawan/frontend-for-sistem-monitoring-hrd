import React from "react";
import { Box, Typography, makeStyles, FormControl, Select, MenuItem } from "@material-ui/core";

import Spacer from "./Spacer";

var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(0.5),
    },
    filterSelect: {
        margin: theme.spacing(0),
        borderRadius: 50 + "pt",
        height: 30 + "pt",
        fontSize: (h * 1.85 / 100) + "px",
    },
    toolbar: {
        borderBottom: "1px solid #e0e0e0",
        height: (h * 5 / 100) + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(4, 0),
    },
}));

function DataTablePagination(){
    const classes = useStyles();

    return(
        <Box className={classes.toolbar}>
            <Typography>Baris Per Halaman</Typography>
            {Spacer("15px", "100%")}
            <FormControl variant="outlined" className={classes.formControl}>
                <Select
                    //value={sortBy}
                    //onChange={handleChangeSortBy}
                    name="pagePerTable"
                    displayEmpty
                    className={classes.filterSelect}
                >
                    <MenuItem>5</MenuItem>
                </Select>
            </FormControl>
        </Box>
    );
}

export default DataTablePagination;
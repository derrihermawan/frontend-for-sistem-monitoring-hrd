import React from "react";

function Spacer(width, height, bgColor, marginL, marginR){
    return(
        <div style={{width: width, height: height, backgroundColor: bgColor, marginLeft: marginL, marginRight: marginR}}>&nbsp;</div>
    );
}

export default Spacer;
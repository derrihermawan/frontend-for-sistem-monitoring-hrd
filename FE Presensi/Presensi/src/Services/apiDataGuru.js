import Axios from "axios";
import config from "./_config";

const CancelToken = Axios.CancelToken;

let cancel;

function getData(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/api/guru/view?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function postData(requestBody){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        //cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.post(`${config.BASE_URL}/api/guru/create`, requestBody, axiosConfig);
}

function putData(id, requestBody) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        //cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.put(`${config.BASE_URL}/api/guru/update?id=${id}`, requestBody, axiosConfig);
}

function deleteData(id){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        //cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.delete(`${config.BASE_URL}/api/guru/delete?id=${id}`, axiosConfig);
}

export { cancel, getData, deleteData, postData, putData };
import Axios from "axios";
import config from "./_config";

const CancelToken = Axios.CancelToken;

let cancel;

function getData(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/api/employeeDetail/view?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function postData(data){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.post(`${config.BASE_URL}/api/employee/create`, data, axiosConfig);
}

function getEditableDetails(filter, order, skip, top){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.get(`${config.BASE_URL}/api/detail-ortu/edit/view?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, null, axiosConfig);
}

function putData(data, id){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.put(`${config.BASE_URL}/api/employee/update/${id}`, data, axiosConfig);
}

function deleteData(id){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.delete(`${config.BASE_URL}/custom/orang-tua/delete/${id}`, null, axiosConfig);
}
export {getData,  postData, putData, deleteData, cancel}; 
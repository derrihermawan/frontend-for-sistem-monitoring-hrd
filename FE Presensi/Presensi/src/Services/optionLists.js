import Axios from "axios";
import config from "./_config";

const CancelToken = Axios.CancelToken;

let cancel;

function getOlDataSiswa(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/detail-siswa/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getOlDataOrtu(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/detail-orangtua/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getOlDataPrestasi(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/menu-dropdown-prestasi/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getOlDataLomba(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/select-lomba/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, null, axiosConfig);
}

function getOlDataAlumni(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/alumni/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getOlDataMapel(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/data-mapel-filsort/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getOlDataRombel(filter, order, skip, top) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/menu-rombel/optionList?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getOlAvSiswa(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-ortu/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/siswa/txt/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvSiswaImage(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-ortu/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/siswa/img/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvOrangtua(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-ortu/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/ortu/txt/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvGuru(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-guru/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/guru/txt/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvGuruImage(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-guru/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/guru/img/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvPembinaEkskul(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-guru/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/pengajar-eskul/txt/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvPembinaEkskulImage(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-guru/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/pengajar-eskul/img/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}
function getOlAvMapel(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-guru/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/mapel/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlAvRuangan(filter) {
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    //return Axios.get(`${config.BASE_URL}/ol/list-guru/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
    return Axios.get(`${config.BASE_URL}/olist/ruangan/view?filter=${filter}&orderby&top&skip`, axiosConfig);
}

function getOlNamaRombel(){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/nama-rombel/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
}

function getOlDataEkskul(){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/eskul/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
}

function getOlDataPengajarEkskul(){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/pengajar-eskul/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
}

function getOlDataTataUsaha(){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/pegawai-tu/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
}

function getOlAdministrasi(){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/administrasi-tu/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
}

function getOlKatalogBuku(){
    let axiosConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/ol/katalog-buku/optionList?filter=&orderby=&skip=&top=`, axiosConfig);
}
export { getOlDataSiswa, getOlDataOrtu, getOlDataPrestasi, getOlDataLomba, getOlDataAlumni, getOlDataMapel, getOlDataRombel, getOlAvSiswa, getOlAvSiswaImage, getOlAvOrangtua, getOlAvGuru, getOlAvGuruImage, getOlAvPembinaEkskul, getOlAvPembinaEkskulImage, getOlAvMapel, getOlAvRuangan, getOlNamaRombel, getOlDataEkskul, getOlDataPengajarEkskul, getOlDataTataUsaha, getOlAdministrasi, getOlKatalogBuku, cancel };

import Axios from "axios";
import config from "./_config";

const CancelToken = Axios.CancelToken;

let cancel;

let axiosConfig = {
    //timeout: 10000,
    headers: {
        "Content-Type": "application/json",
        //"Access-Control-Allow-Origin": "*",
    },
    cancelToken: new CancelToken(c => { cancel = c }),
}

function getProvinsi() {
    return Axios.get(`${config.BASE_URL}/api/daerah-indonesia/provinsi`, axiosConfig);
}
function getKabupaten(id) {
    return Axios.get(`${config.BASE_URL}/api/daerah-indonesia/provinsi/${id}/kabupaten`, axiosConfig);
}
function getKecamatan(id) {
    return Axios.get(`${config.BASE_URL}/api/daerah-indonesia/provinsi/kabupaten/${id}/kecamatan`, axiosConfig);
}
function getDesa(id) {
    return Axios.get(`${config.BASE_URL}/api/daerah-indonesia/provinsi/kabupaten/kecamatan/${id}/desa`, axiosConfig);
}

export {getProvinsi, getKabupaten, getKecamatan, getDesa, cancel};
import Axios from "axios";
import config from "./_config";

const CancelToken = Axios.CancelToken;

let cancel;

function getData(filter, order, skip, top){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/api/pengajar-eskul/view?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function getDetails(filter, order, skip, top, id){
    let axiosConfig={
        ////timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        },
        cancelToken: new CancelToken(c => { cancel = c }),
    }
    return Axios.get(`${config.BASE_URL}/api/pengajar-eskul/list-eskul/${id}/view?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosConfig);
}

function postData(data){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.post(`${config.BASE_URL}/api/employee/create`, data, axiosConfig);
}

function getEditableDetails(filter, order, skip, top){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.get(`${config.BASE_URL}/api/detail-ortu/edit/view?filter=${filter}&orderby=${order}&skip=${skip}&top=${top}`, null, axiosConfig);
}

function putData(data, id){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.put(`${config.BASE_URL}/custom/pengajar-eskul/update/${id}`, data, axiosConfig);
}

function deleteData(id){
    let axiosConfig={
        //timeout:10000,
        withCredentials:true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.delete(`${config.BASE_URL}/custom/pengajar-eskul/delete/${id}`, null, axiosConfig);
}
export {getData, getDetails, postData, getEditableDetails, putData, deleteData, cancel};
import Axios from "axios";
import config from "./_config";

const CancelToken = Axios.CancelToken;

let cancel;
let axiosGeneralConfig = {
    //timeout: 10000,
    withCredentials: true,
    headers: {
        "Content-Type": "application/json",
    },
}

function getData(date, filter, order, skip, top) {
    let axiosFetchConfig = {
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
        //cancelToken: fetchSource,
        cancelToken: new CancelToken(c => { cancel = c }),
    }
       console.log("search "+filter)
        return Axios.get(`${config.BASE_URL}/api/lateDetail/view?filter=${date} ${filter}&orderby=${order}&skip=${skip}&top=${top}`, axiosFetchConfig);
    
    
}


export { getData, cancel};
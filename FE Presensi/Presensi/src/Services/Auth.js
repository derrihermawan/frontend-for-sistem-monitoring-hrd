import Axios from "axios";
import config from "./_config";

function login(requestBody){
    let axiosConfig={
        //timeout: 10000,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        }
    }
    return Axios.post(`${config.BASE_URL}/account/loginv2`, requestBody, axiosConfig);
}

function generateUuid(){
    const uuidv5 = require('uuid/v5');

    var nav = window.navigator;
    var screen = window.screen;
    var guid = nav.mimeTypes.length;

    guid += nav.userAgent.replace(/\D+/g, '');
    guid += nav.plugins.length;
    guid += screen.availHeight * (window.devicePixelRatio) ? window.devicePixelRatio : 1 || '';
    guid += screen.availWidth * (window.devicePixelRatio) ? window.devicePixelRatio : 1 || '';
    guid += screen.height * (window.devicePixelRatio) ? window.devicePixelRatio : 1 || '';
    guid += screen.width * (window.devicePixelRatio) ? window.devicePixelRatio : 1 || '';
    guid += screen.pixelDepth || '';

    var uuid=uuidv5(guid, uuidv5("cv.tujuhsembilan.alazhar", uuidv5.DNS));

    return uuid;
}

export {login, generateUuid};
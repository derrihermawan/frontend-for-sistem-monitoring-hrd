import React, {useState} from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/InputBase';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';

import AccountCircle from '@material-ui/icons/AccountCircle';
import LockIcon from '@material-ui/icons/Lock';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import Swal from 'sweetalert2';

import * as Auth from "./../Services/Auth";
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/styles';
import { CircularProgress, LinearProgress } from '@material-ui/core';

//var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

const useStyles = makeStyles({
    coloring: {
        backgroundColor: "#333399",
    },
    card: {
        minWidth: 300+"pt",
        maxWidth: 25+"%",
        minHeight: 300+"pt",
        maxHeight: 50+"%",
        marginLeft: 50+"%",
        transform: "translate(-50%, 0)",
        marginTop: (Math.max(h*15/100))+"px",
        overflow: "visible",
        border: "2px solid",
        borderColor: "#ff9900",
        backgroundColor: "#333399",
    },
    circleLogo: {
        position: "absolute",
        height: 100+"pt",
        width: 100+"pt",
        backgroundColor: "#333399",
        left: 50+"%",
        top: "-24pt",
        transform: "translate(-50%, -25%)",
    },
    logoImage: {
        height: 100+"%",
        width: 100+"%",
    },
    title: {
        textAlign: "center",
        fontSize: 28+"pt",
        fontFamily: "Muli",
        marginTop: 50+"pt",
    },
    textFieldContainer:{
        paddingRight: 10+"pt",
        paddingLeft: 10 + "pt",
        marginTop: 5+"pt",
        height: 40+"pt",
        width: 85 + "%",
        border: "1px solid",
        borderRadius: 500+"pt",
        borderColor: "#ddd",
        marginLeft: 50+"%",
        transform: "translate(-50%, 0%)",
    },
    textFieldGrid: {
        height: 100+"%",
        width: 80 + "%",
    },
    textFieldIconGrid: {
        height: 100 + "%",
        width: 10 + "%",
    },
    textField: {
        height: 100 + "%",
        width: 100 + "%",
    },
    textFieldIcon: {
        height: 100 + "%",
        width: 85 + "%",
        color: "#0182C6",
    },
    buttonSubmit: {
        marginLeft: 50+"%",
        borderRadius: 500+"pt",
        transform: "translate(-50%, 0%)",
        borderColor: "#0182C6",
        color: "#0182C6",
        minWidth: 100+"pt",
        minHeight: 30+"pt",
        height: 35+"pt",
    },
    forgotPasswordLink: {
        marginTop: 20+"pt",
        marginLeft: 50 + "%",
        transform: "translate(-50%, 0%)",
        color: "#888",
        fontSize: 12+"pt",
        fontFamily: "Muli",
    },
    copyrightNotice: {
        position: "absolute",
        fontFamily: "Muli",
        fontSize: 10+"pt",
        color: "#888",
        textAlign: "center",
        top: h-(h*8.5/100)+"px",
        left: 50+"%",
        transform: "translate(-50%, 0%)",
    },
    loadingIcon: {
        animation: "spin-ccw 1s linear infinite",
    },
    cursorPointer: {
        cursor: "pointer",
    },
});

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 5000
});

const useForm = (callback) => {
    const [inputs, setInputs] = useState({
        "username": "",
        "password": "",
    });
    const [controlsDisabledState, setControlsDisabledState] = useState({});
    const [redirect, setRedirect]=useState({});

    const handleSubmit = (event) => {
        if (event) {
            event.preventDefault();
        }

        callback(inputs, setControlsDisabledState, setRedirect);
    }

    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
    }

    return {
        handleSubmit,
        handleInputChange,
        inputs,
        controlsDisabledState,
        redirect, setRedirect,
        setControlsDisabledState
    };
}

function doLogin(requestBody, setControlsDisabledState, setRedirect) {
    setControlsDisabledState(controlsDisabledState => ({ ...controlsDisabledState, "submitButton": true }));

    Auth.login(requestBody).then(response => {
        if(response){
            if (response.data.httpStatusCode === 202 && response.data.isLoggedIn){
                Toast.fire({
                    type: "success",
                    title: "Login Sukses!"
                });
                setRedirect(redirect=>({...redirect, "redirectPage": "/dashboard", "shouldRedirect": true}));
            }else{
                Toast.fire({
                    type: "error",
                    title: "Login Gagal!"
                });
            }
        }else{
            Toast.fire({
                type: "error",
                title: "Cek Koneksi Internet!"
            });
        }
    }).catch(error => {
        if(error.response.status===404){
            Toast.fire({
                type: "error",
                title: "Halaman Tidak Ditemukan!"
            });
        }else{
            Toast.fire({
                type: "error",
                title: error.response.data.errorMessage,
                text: error.response.data.suggestionMessage,
            });
        }
    }).finally(()=>{
        setControlsDisabledState(controlsDisabledState => ({ ...controlsDisabledState, "submitButton": false }));
    });
}

const CustomCircularProgress = withStyles({
    root: {
        color: '#517296',
    },
})(CircularProgress);
const CustomLinearProgress = withStyles({
    colorPrimary: {
        height: 4 + "px",
        borderRadius: "0px 0px 4px 4px",
        backgroundColor: '#d3d3d3',
    },
    barColorPrimary: {
        borderRadius: 25 + "px",
        backgroundColor: '#517296',
    },
})(LinearProgress);

// The Login Component
function Login() {
    const classes = useStyles();
    const {inputs, controlsDisabledState, setControlsDisabledState, handleSubmit, handleInputChange, redirect} = useForm(doLogin);

    const handleClickShowPassword = () => {
        setControlsDisabledState(controlsDisabledState => ({ ...controlsDisabledState, "showPassword": !controlsDisabledState.showPassword }));
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };

    inputs.uuid = Auth.generateUuid();

    // Debug
    const [toDashboard, setToDashboard]=useState(false);

    return (
        
        <Container maxWidth="xl" >
            {/* DEBUGGING */(toDashboard) ? <Redirect to="/dashboard" /> : null}

            {(redirect.shouldRedirect)?<Redirect to={redirect.redirectPage}/>:null}
            
            <Card className={classes.card}>
                <CardContent>
                    <Card className={classes.circleLogo}>
                        <CardContent>
                            <img className={classes.logoImage} src={process.env.PUBLIC_URL+'/logotujuhsembilan.png'} alt="Logo Sekolah Al Azhar" />
                        </CardContent>
                    </Card>
                    <Typography className={classes.title}>Selamat Datang</Typography>
                    <Grid container spacing={0} alignItems="flex-end" className={classes.textFieldContainer} style={{marginTop: 25+"pt"}}>
                        <Grid item className={classes.textFieldIconGrid}>
                            <AccountCircle className={classes.textFieldIcon} />
                        </Grid>
                        <Grid item className={classes.textFieldGrid}>
                            <TextField
                                id="username-input"
                                label="Username"
                                placeholder="Username"
                                className={classes.textField}
                                onChange={handleInputChange}
                                name="username"
                                value={inputs.username}
                            />
                        </Grid>
                    </Grid>
                    <Grid container spacing={0} alignItems="flex-end" className={classes.textFieldContainer}>
                        <Grid item className={classes.textFieldIconGrid}>
                            <LockIcon className={classes.textFieldIcon} />
                        </Grid>
                        <Grid item className={classes.textFieldGrid}>
                            <TextField
                                id="password-input"
                                label="Password"
                                placeholder="Password"
                                className={classes.textField}
                                type={controlsDisabledState.showPassword ? 'text' : 'password'}
                                onChange={handleInputChange}
                                name="password"
                                value={inputs.password}
                            />
                        </Grid>
                        <Grid item className={classes.textFieldIconGrid}>
                            {controlsDisabledState.showPassword ? <Visibility className={classes.textFieldIcon + " " + classes.cursorPointer} onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword} /> : <VisibilityOff className={classes.textFieldIcon + " " + classes.cursorPointer} onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword} />}
                        </Grid>
                    </Grid>
                    <CardActions style={{marginTop: 20+"pt"}}>
                        <Button
                            variant="outlined" size="large"
                            className={classes.buttonSubmit}
                            onClick={()=>{
                                //* DEBUGGING
                                setControlsDisabledState(controlsDisabledState => ({ ...controlsDisabledState, "submitButton": true }));

                                setTimeout(()=>{
                                    Toast.fire({
                                        type: "success",
                                        title: "Login Sukses!"
                                    });
                                }, 2000);

                                setTimeout(()=>{
                                    setToDashboard(true)
                                }, 3750);
                                //*/

                                //handleSubmit();
                            }}
                            disabled={controlsDisabledState.submitButton}
                        >{(controlsDisabledState.submitButton) ?/*<Sync className={classes.loadingIcon}/>*/ <CustomCircularProgress size={25} thickness={4} />:"masuk"}</Button>
                    </CardActions>
                </CardContent>
                {(controlsDisabledState.submitButton) ?/*<Sync className={classes.loadingIcon}/>*/ <CustomLinearProgress style={{marginTop: 5+"px"}} /> : null}
            </Card>
            <Link component="button" href={null} className={classes.forgotPasswordLink}>
                Lupa Password?
            </Link>
            <Typography className={classes.copyrightNotice}>
                Sistem Informasi Manajemen Administrasi Pendidikan Terpadu<br/>
                Al Azhar Muhajirien - Copyright © 2019, CV Tujuh Sembilan
            </Typography>
        </Container>
    );
}

export default Login;
import React, {useState} from 'react';
import clsx from 'clsx';

import {Link, Redirect} from "react-router-dom";
import {ContentRoutes} from "./../Routes";

import {makeStyles, withStyles, lighten} from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import GroupIcon from '@material-ui/icons/Group';
import NotificationsIcon from '@material-ui/icons/Notifications';
import SettingsIcon from '@material-ui/icons/Settings';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import { Box, Grid, Avatar, IconButton, Tooltip, MenuItem, Menu } from '@material-ui/core';

import * as Auth from "./../Services/Auth";
import Spacer from "./../Components/Spacer";


const drawerWidth = 240;
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        fontFamily: "Muli",
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        backgroundColor: "#002f48",
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        overflowX: 'hidden',
        backgroundColor: "#002f48",
        //color: "#0182C6",
        color: "#517296",
        fontFamily: "Muli",
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(8) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(8) + 1,
        },
        backgroundColor: "#002f48",
        //color: "#0182C6",
        color: "#517296",
        fontFamily: "Muli",
    },
    logoImage: {
        height: 35 + "pt",
        width: 35 + "pt",
    },
    logoText: {
        marginLeft: 15+"pt",
        color: "#ddd",
    },
    logoBannerOpen: {
        height: 75+"px",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    logoBannerClose: {
        height: 65 + "px",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    navigationList: {
        //padding: theme.spacing(0, 0),
    },
    listButton: {
        height: 35+"pt",
        padding: theme.spacing(0, 2.5),
    },
    activeListButton: {
        backgroundColor: "rgba(255, 255, 255, 0.05)",
        color: "#b1d2f6",
        borderLeft: "3px solid #b1d2f6",
    },
    listButtonIcon: {
        //color: "#0182C6",
        color: "#517296",
        height: theme.spacing(3),
        width: theme.spacing(3),
    },
    listButtonText: {
        display: "block",
    },
    contentContainer: {
        flexGrow: 1,
    },
    tools: {
        paddingRight: theme.spacing(2),
    },
    toolbarOpen: {
        background: `url("${process.env.PUBLIC_URL}/bg_cartograph_main.png")`,
        borderBottom: "1px solid #e0e0e0",
        height: 75+"px",
        width: 100+"%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(0, 0),
        ...theme.mixins.toolbar,
    },
    toolbarClose: {
        background: `url("${process.env.PUBLIC_URL}/bg_cartograph_main.png")`,
        borderBottom: "1px solid #e0e0e0",
        height: 65 + "px",
        width: 100 + "%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(0, 0),
        ...theme.mixins.toolbar,
    },
    controlButtonOpen: {
        transition: theme.transitions.create(['left', 'top'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        position: "fixed",
        backgroundColor: "#103f58",
        color: "#ddd",
        borderRadius: "0px 5px 5px 0px",
        left: drawerWidth,
        top: 15 + "px",
        minWidth: 15+"pt",
        maxWidth: 15+"pt",
        height: 45+"px",
        zIndex: 500,
    },
    controlButtonClose: {
        transition: theme.transitions.create(['left', 'top'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        position: "fixed",
        backgroundColor: "#103f58",
        color: "#ddd",
        borderRadius: "0px 5px 5px 0px",
        left: theme.spacing(8) + 1,
        top: 10 + "px",
        minWidth: 15 + "pt",
        maxWidth: 15 + "pt",
        height: 45 + "px",
    },
    contentOpen: {
        //backgroundColor: "#333",
        //background: "url('http://api.thumbr.it/whitenoise-256x256.png?background=f7f8fcff&noise=c9c9c9&density=35&opacity=15')",
        background: `url("${process.env.PUBLIC_URL}/bg_main_noise.png")`,
        padding: theme.spacing(0, 3),
        height: h-75+"px",
    },
    contentClose: {
        //backgroundColor: "#333",
        //background: "url('http://api.thumbr.it/whitenoise-256x256.png?background=f7f8fcff&noise=c9c9c9&density=35&opacity=15')",
        background: `url("${process.env.PUBLIC_URL}/bg_main_noise.png")`,
        padding: theme.spacing(0, 3),
        height: h-65+"px",
    },
    pageTitle: {
        marginLeft: 30+"pt",
        fontSize: 24+"pt",
    },
    avatar: {
        border: "2pt solid #fff",
        margin: 10,
        marginRight: theme.spacing(2),
        width: 45,
        height: 45,
        color: '#fff',
        backgroundColor: "#323232",
        fontSize: 20+"px",
    },
    boxShadow: {
        boxShadow: "0 0 8.5px 1px rgba(0, 0, 0, 0.75)",
    },
}));

// Components
const SidebarTooltip = withStyles(theme => ({
    tooltip: {
        //backgroundColor: theme.palette.common.white,
        color: lighten('#b1d2f6', 0.5),
        backgroundColor: '#002f48',
        boxShadow: theme.shadows[2],
        fontSize: 13,
        marginLeft: 10+"px",
    },
}))(Tooltip);
const NotificationList = withStyles(theme=>({
    paper: {
        boxShadow: theme.shadows[7],
        border: '1px solid #d3d4d5',
    },
}))(props => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));
const NotificationListItem = withStyles(theme => ({
    root: {
        '&:focus': {
            backgroundColor: theme.palette.grey[300],
            '& .MuiListItemIcon-root, & .MuiListItemText-primary, & .MuiTypography-root': {
                //color: theme.palette.common.black,
            },
        },
    },
}))(MenuItem);
// Components

const useNavigation = (callback) => {
    const [activePage, setActivePage] = useState("Presensi");

    const handlePageChange = (event) => {
        event.persist();
        setActivePage(event.target.textContent);
    }

    return {
        handlePageChange,
        activePage,
        setActivePage
    };
}

const NotificationBadge = withStyles(theme => ({
    badge: {
        right: -3,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '5px 5px',
    },
}))(Badge);

function CheckPage() {
    if (true) {
        return <Redirect to='/dashboard/presensi' />
    } else {
        return <Redirect to='/login' />
    }
}

function GeneralLayout() {
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    const {activePage, setActivePage} = useNavigation(null);

    function handleDrawerOpen() {
        setOpen(true);
    }

    function handleDrawerClose() {
        setOpen(false);
    }

    function handlePageChange(pageName){
        setActivePage(pageName)
    }

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleNotificationOpen = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleNotificationClose = () => {
        setAnchorEl(null);
    };

    const NOTIF_ITEM_HEIGHT = 48;

    // Dummy
    var badgeNotificationCount = Math.ceil(Math.random() * 1024);

    return (
        <div className={classes.root}>
            <CheckPage /> <CssBaseline />
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx(classes.boxShadow, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
                open={open}
            >
                <div className={(open)?classes.logoBannerOpen:classes.logoBannerClose}>
                    <img className={classes.logoImage} src={process.env.PUBLIC_URL + '/usericon.png'} alt="Logo Sekolah Al Azhar" />
                    <Typography className={(open)?classes.logoText:classes.hide}>HRD<br/>Tujuh Sembilan</Typography>
                </div>
                <Divider style={{ backgroundColor: "#214266"}} />
                <List className={classes.navigationList}>
                    <SidebarTooltip disableHoverListener={open} disableFocusListener={open} disableTouchListener title="Presensi" placement="right">
                        <ListItem component={Link} to="/dashboard/presensi" className={classes.listButton+((activePage==="Presensi")?" "+classes.activeListButton:"")} button onClick={()=>{handlePageChange("Presensi")}}>
                            <ListItemIcon className={classes.listButtonIcon} >{<AssignmentIcon className={classes.listButtonIcon} />}</ListItemIcon>
                            <ListItemText className={(open) ? classes.listButtonText : classes.hide} primary="Presensi" />
                        </ListItem>
                    </SidebarTooltip>
                    <SidebarTooltip disableHoverListener={open} disableFocusListener={open} disableTouchListener title="Kepegawaian" placement="right">
                        <ListItem component={Link} to="/dashboard/kepegawaian" className={classes.listButton + ((activePage === "Kepegawaian") ? " " + classes.activeListButton : "")} button onClick={() => { handlePageChange("Kepegawaian") }}>
                            <ListItemIcon className={classes.listButtonIcon} >{<GroupIcon className={classes.listButtonIcon} />}</ListItemIcon>
                            <ListItemText className={(open) ? classes.listButtonText : classes.hide} primary="Kepegawaian" />
                        </ListItem>
                    </SidebarTooltip>
                    <SidebarTooltip disableHoverListener={open} disableFocusListener={open} disableTouchListener title="Rekapan" placement="right">
                        <ListItem component={Link} to="/dashboard/Rekapan" className={classes.listButton + ((activePage === "Rekapan") ? " " + classes.activeListButton : "")} button onClick={() => { handlePageChange("Rekapan") }}>
                            <ListItemIcon className={classes.listButtonIcon} >{<LibraryBooksIcon className={classes.listButtonIcon} />}</ListItemIcon>
                            <ListItemText className={(open) ? classes.listButtonText : classes.hide} primary="Rekapan" />
                        </ListItem>
                    </SidebarTooltip>
                   
                </List>
                <Divider style={{ backgroundColor: "#214266" }} />
                <List className={classes.navigationList}>
                    <SidebarTooltip disableHoverListener={open} disableFocusListener={open} disableTouchListener title="Pengaturan" placement="right">
                        <ListItem component={Link} to="/dashboard/pengaturan" className={classes.listButton + ((activePage === "Pengaturan") ? " " + classes.activeListButton : "")} button onClick={() => { handlePageChange("Pengaturan") }}>
                            <ListItemIcon className={classes.listButtonIcon} >{<SettingsIcon className={classes.listButtonIcon} />}</ListItemIcon>
                            <ListItemText className={(open) ? classes.listButtonText : classes.hide} primary="Pengaturan" />
                        </ListItem>
                    </SidebarTooltip>
                </List>
            </Drawer>
            <SidebarTooltip disableTouchListener title={(open)?"Tutup Sidebar":"Buka Sidebar"} placement="bottom-start">
            <Button className={(open) ? clsx(classes.controlButtonOpen, "bg-blue-light") : clsx(classes.controlButtonClose, "bg-blue-light")} onClick={(open) ? handleDrawerClose : handleDrawerOpen}>
                {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </Button>
            </SidebarTooltip>
            <main className={classes.contentContainer}>
                <Box className={(open)?classes.toolbarOpen:classes.toolbarClose}>
                    <Box flexGrow={1}>
                        <Typography className={classes.pageTitle}>{(activePage) ? activePage : "Presensi"}</Typography>
                    </Box>
                    <Box className={classes.tools}>
                        <Grid container justify="center" alignItems="center">

                            <SidebarTooltip
                                disableTouchListener
                                title={(badgeNotificationCount===0)?"Tidak Ada Pemberitahuan":((badgeNotificationCount<512)?badgeNotificationCount:512+"+")+" Pemberitahuan"}
                                placement="left">
                                <IconButton onClick={handleNotificationOpen}>
                                    <NotificationBadge badgeContent={badgeNotificationCount} max={64} variant="standard" color="error">
                                        <NotificationsIcon />
                                    </NotificationBadge>
                                </IconButton>
                            </SidebarTooltip>
                            <NotificationList
                                id="customized-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleNotificationClose}
                                PaperProps={{
                                    style: {
                                        maxHeight: NOTIF_ITEM_HEIGHT * 4.5,
                                        width: 300,
                                    },
                                }}
                            >
                                <NotificationListItem>
                                    <ListItemIcon>
                                        <NotificationImportantIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit" noWrap>
                                        {"An Important Notification"}
                                    </Typography>
                                </NotificationListItem>
                                <NotificationListItem>
                                    <ListItemIcon>
                                        <NotificationsIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit" noWrap>
                                        {"A Normal Notification"}
                                    </Typography>
                                </NotificationListItem>
                                <NotificationListItem>
                                    <ListItemIcon>
                                        <NotificationsIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit" noWrap>
                                        {"A Normal Notification with Long Text"}
                                    </Typography>
                                </NotificationListItem>
                                <NotificationListItem>
                                    <ListItemIcon>
                                        <NotificationsIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit" noWrap>
                                        {"Yet Another Notification"}
                                    </Typography>
                                </NotificationListItem>
                                <NotificationListItem>
                                    <ListItemIcon>
                                        <NotificationsIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit" noWrap>
                                        {"Yet Another Notification"}
                                    </Typography>
                                </NotificationListItem>
                                <NotificationListItem>
                                    <ListItemIcon>
                                        <NotificationsIcon fontSize="small" />
                                    </ListItemIcon>
                                    <Typography variant="inherit" noWrap>
                                        {"Yet Another Notification"}
                                    </Typography>
                                </NotificationListItem>
                            </NotificationList>

                            {Spacer("2px", "35px", "#e5e5e5", "20px", "20px")}
                            <Typography style={{fontSize: 10+"pt"}}> { Auth.generateUuid() } </Typography>
                            <Avatar src={"https://api.adorable.io/avatars/200/" + Auth.generateUuid()} className={classes.avatar} />
                        </Grid>
                    </Box>
                </Box>
                <Container fixed={false} maxWidth={false} className={(open)?classes.contentOpen:classes.contentClose}>

                    <ContentRoutes/>

                </Container>
            </main>
        </div>
    );
}

export default GeneralLayout;

/*
// Close Open Button
<div className={classes.toolbar}>
    <IconButton onClick={(open) ? handleDrawerClose : handleDrawerOpen}>
        {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
    </IconButton>
</div>
*/
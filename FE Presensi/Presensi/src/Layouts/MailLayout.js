import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(8) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(8) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    logoImage: {
        height: 35 + "pt",
        width: 35 + "pt",
    },
    logoText: {
        marginLeft: 15 + "pt",
    },
    logoBannerOpen: {
        height: 75 + "pt",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    logoBannerClose: {
        height: 50 + "pt",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    navigationList: {
        //padding: theme.spacing(0, 0),
    },
    listButton: {
        height: 35 + "pt",
        padding: theme.spacing(0, 2.5),
    },
    listButtonIcon: {
        height: theme.spacing(3),
        width: theme.spacing(3),
    },
    listButtonText: {
        display: "block",
    },
}));

function MailLayout() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);

    function handleDrawerOpen() {
        setOpen(true);
    }

    function handleDrawerClose() {
        setOpen(false);
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
                open={open}
            >
                <div className={(open) ? classes.logoBannerOpen : classes.logoBannerClose}>
                    <img className={classes.logoImage} src={process.env.PUBLIC_URL + '/al-azhar.png'} alt="Logo Sekolah Al Azhar" />
                    <Typography className={(open) ? classes.logoText : classes.hide}>Tata Usaha<br />Al Azhar</Typography>
                </div>
                <Divider />
                <List className={classes.navigationList}>

                    <ListItem className={classes.listButton} button key="Mail">
                        <ListItemIcon className={classes.listButtonIcon} >{<MailIcon className={classes.listButtonIcon} />}</ListItemIcon>
                        <ListItemText className={(open) ? classes.listButtonText : classes.hide} primary="Mail" />
                    </ListItem>

                </List>
                <Divider />
            </Drawer>
            <main className={classes.content}>
            </main>
        </div>
    );
}

export default MailLayout;

/*
// Close Open Button
<div className={classes.toolbar}>
    <IconButton onClick={(open) ? handleDrawerClose : handleDrawerOpen}>
        {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
    </IconButton>
</div>
*/
import React from "react";
import { Route, Switch } from "react-router-dom";

// Welcome Checker
import CheckPage from "./CheckPage";

// Layouts
import Login from "./Layouts/Login";
import GeneralLayout from "./Layouts/GeneralLayout";

// Views or Pages
import Presensi from "./Views/Presensi";
import Kepegawaian from "./Views/Kepegawaian";
import Rekapan from "./Views/Rekapan"
// Error Page and Miscellaneous
import ErrorPage from "./Views/_ErrorPage";

class LayoutRoutes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" name="checkPage" component={CheckPage}/>
        <Route exact path="/login" name="loginPage" component={Login} />
        <Route path="/dashboard" name="GeneralLayout" component={GeneralLayout} />
        <Route component={ErrorPage}/>
      </Switch>
    );
  }
}

class ContentRoutes extends React.Component {
  render(){
    return(
      <Switch>
        <Route exact path="/dashboard/" name="Presensi" component={Presensi} />
        <Route path="/dashboard/Presensi" name="Presensi" component={Presensi} />
        <Route path="/dashboard/kepegawaian" name="Kepegawaian" component={Kepegawaian} />
        <Route path="/dashboard/Rekapan" name="Rekapan" component={Rekapan} />
        <Route component={ErrorPage} />
      </Switch>
    );
  }
}

export {LayoutRoutes, ContentRoutes};
